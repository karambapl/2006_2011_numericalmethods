/*
 *	About : 
 *	2009. One of my project in Cz�stochowa University Of Technology
 *
 *	The final project on the V-th semester 
 *	(Analysing, Design and the Object Programming)
 *	
 *	The study about utilization of numerical methods 
 *	for resolving physical body simulation.
 *	This version contains only gravitational forces between body.
 *      
 *	See:
 *	Command line manual and examples.
 *	
 *	
 *	Copyright (C) 2009, 2017  Rafa� Sta�czuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <GL/glut.h>
#include <time.h>



using namespace std;



    /*!
     Skala Makro�wiata 10e8 - znaczy to, 
       �e jedno oczko siatki, zdefiniowanej w klasie Csiatka, 
      ma 100000km szeroko�ci i wysoko�ci
    */
    const double skala=10e8;
    int window;
    ///Zmienna przechowuj�ca stan naci�ni�tego klawisza myszki
    int button_state = GLUT_DOWN;
    ///Aktualne polozenie myszki
    int button_x,button_y;	

///Funkcja zwracaj�ca komunikat po podaniu kodu b��du
string blad(const int& kod)
{
 string komunikat[]={
        "Za malo parametrow!!!",//0
        "Parametry -s i -k wymagaj� warto�ci!!!",//1
        }     ; 
   return komunikat[kod];    
       
}

///Pomocnicza funkcja zwracaj�ca opis funkcji klawisza po podaniu numeru indeksu
string help_klawiszy( const int& i )
{
    string opisy[]={  
        "[ESC] : wyjscie",
        "'+'   : zwieksz dokladnosc rysowania sfer",
        "'-'   : zmniejsz dokladnosc rysowania sfer",
        "'.'   : zapisz krok do pliku",
        "'f'   :   sym.kamera_.translated_z()+=0.02 ",
        "'r'   :   sym.kamera_.translated_z()-=0.02 ",
        "'a'   :   sym.kamera_.translated_y()+=0.01",
        "'d'   :   sym.kamera_.translated_y()-=0.01",
        "'w'   :   sym.kamera_.translated_x()+=0.01",
        "'s'   :   sym.kamera_.translated_x()-=0.01",
        "'q'   : pozycja startowa kamery ",
        "'p'   : wlacza rysowanie pola ekwipotencjalnego",   
        "'o'   : sym.pole_.pole_ekwipotencjalne_srodek().z()+=0.02",
        "'l'   : sym.pole_.pole_ekwipotencjalne_srodek().z()-=0.02",
        "'u'   : sym.pole_.pole_ekwipotencjalne_srodek().x()+=0.02",
        "'j'   : sym.pole_.pole_ekwipotencjalne_srodek().x()-=0.02",
        "'i'   : sym.pole_.pole_ekwipotencjalne_srodek().y()+=0.02",
        "'k'   : sym.pole_.pole_ekwipotencjalne_srodek().y()-=0.02",
        "'t'   : sym.pole_.pole_nx()+=1",
        "'g'   : sym.pole_.pole_nx()-=1",
        "'y'   : sym.pole_.pole_ny()+=1",
        "'h'   : sym.pole_.pole_ny()-=1",
        "'v'   : sym.pole_.pole_dx()-=0.01",
        "'b'   : sym.pole_.pole_dx()+=0.01",
        "'n'   : sym.pole_.pole_dy()-=0.01",
        "'m'   : sym.pole_.pole_dy()+=0.01",
        "'c'   : rysowanie siatki na polu o z=0",
        "'x'   : rysowanie wektorow predkosci",
        "'z'   : pauza symulacji",
        "'1'   : sym.klawisze_help_ ",
        "'2'   : sym.dodatkowe_informacje_"};

  return opisy[i];
}             
//========================================================================================
//========================================================================================
///Klasa przechowuj�ca sk�adowe koloru danego obiektu
class c_kolor{
      private:
       
       double r_;
       double g_;
       double b_;
      
      public:
       
       c_kolor():r_( 0.0 ), g_( 0.0 ), b_( 0.0 ){;};      
       c_kolor( const double& _r, const double& _g, const double& _b):r_(_r), g_(_g), b_(_b){;};
       
       double& r( void ){ return r_; };
       const double& r(void) const { return r_;};

       double& g( void ){ return g_; };
       const double& g(void) const { return g_;};

       double& b( void ){ return b_; };
       const double& b(void) const { return b_;};              
      
      };
      
//========================================================================================
//========================================================================================      
///Klasa przechowuj�ca sk�adowe wektora po�o�enia, pr�dko�ci lub przyspieszenia
class c_wektor{
      private:
      
       long double  x_;
       long double  y_;
       long double  z_; 
       
       ///Zmienna przechowuj�ca d�ugo�� wektora 
       long double  dl_;      
      
      public:
       
       c_wektor():x_( 0.0 ), y_( 0.0 ), z_( 0.0 ), dl_( 0.0 ){;};
       c_wektor(const long double& a, const long double& b, const long double& c): x_(a), y_(b), z_(c)
       {};
       
       void wprowadz(const long double& a, const long double& b, const long double& c)
       {
         x_=a;
         y_=b;
         z_=c;   
       }
       
       /*!
       * Liczy d�ugo�� wektora
       * Raz policzona d�ugo�� wektora jest w jej klasie przechowywana do ko�ca jej istnienia lub nadpisania
       */
       void dlugosc_licz(void)
       {
        dl_=sqrt( x_*x_ + y_*y_ + z_*z_ );
       }; 
       
       void zeruj(void)
       { x_=0.0;
         y_=0.0;
         z_=0.0;
       };
       
       ///Przekazuje d�ugo�� wektora uprzednio policzonego
       const  long double& dlugosc( void ) const { return dl_;};
       
       long double& x( void ){ return x_; };
       const long double& x( void ) const { return x_;};
       
       long double& y( void ){ return y_; };
       const long double& y( void ) const { return y_; };
       
       long double& z( void ){ return z_; };
       const long double& z( void ) const { return z_; };
        
        ///Iloczyn wektorowy
        friend c_wektor operator *( const c_wektor& , const c_wektor& );
        ///Iloraz wektora i skalara
        friend c_wektor operator /( const c_wektor& , const long double& );
        ///Iloraz skalaru i wektoru 
        friend c_wektor operator /( const long double&, const c_wektor&  );
        ///Iloczyn skalarny wektor�w                
        friend long double operator %( const c_wektor& , const c_wektor& );
        ///Iloczyn skalara i wektora      
        friend c_wektor operator *( const long double& , const c_wektor& );
        ///Iloczyn wektora i skalara        
        friend c_wektor operator *( const c_wektor& , const long double& );
        ///Suma dw�ch wektor�w
        friend c_wektor operator +(  const c_wektor& , const c_wektor& );
        ///R�nica dw�ch wektor�w
        friend c_wektor operator -(  const c_wektor& , const c_wektor& );
         ///Operator przyr�wnania dw�ch wektor�w
         c_wektor& operator =(  const c_wektor&  );       
         
      
      };

     
c_wektor& c_wektor::operator =( const c_wektor& w )
{
 if( &w!=this )
     {
      x_=w.x();
      y_=w.y();
      z_=w.z();
     }         
 return *this;         
          
}

c_wektor operator /( const c_wektor& a, const long double& b)
{
         return c_wektor( ( a.x() / b ), ( a.y() / b ), ( a.z() / b ) );
}

c_wektor operator /(  const long double& a, const c_wektor& b)
{
         return c_wektor( ( a / b.x() ), ( a / b.y() ), ( a / b.z() ) );
}

c_wektor operator *( const c_wektor& w, const long double& a)
{
         return c_wektor( a*w.x(), a*w.y(), a*w.z() );
}

c_wektor operator *(  const long double& a, const c_wektor& w)
{
         return c_wektor( a*w.x(), a*w.y(), a*w.z() );
}

c_wektor operator *( const c_wektor& a, const c_wektor& b )
{          
          return c_wektor( a.y()*b.z() - a.z()*b.y(), a.z()*b.x() - a.x()*b.z(), a.x()*b.y() - a.y()*b.x() );
}


     
long double operator %( const c_wektor& a, const c_wektor& b)//iloczyn skalarny wektor�w
{
 return ( a.x()*b.x() + a.y()*b.y() + a.z()*b.z() );      
}

c_wektor operator +(  const c_wektor& a, const c_wektor& b)
{
 return c_wektor( a.x()+b.x(), a.y()+b.y(), a.z()+b.z() );         
}

c_wektor operator -(  const c_wektor& a, const c_wektor& b)
{
 return c_wektor( a.x()-b.x(), a.y()-b.y(), a.z()-b.z() );            
}
        
        
//========================================================================================
//========================================================================================    

//========================================================================================
//========================================================================================

///Klasa zawieraj�ca parametry danego obiektu materialnego
class materia_kulista{
      private:
       ///Wsp�rz�dne kartezja�skie
       c_wektor    w_;
       ///Wektor pr�dko�ci
       c_wektor    v_;
       
       long double masa_;
       c_kolor     kolor_;
       
       double      promien_;
      
      public:
       
       materia_kulista(): masa_( 0.0 ), promien_( 0.0 ){;}
         
       materia_kulista( const long double& masa__, const c_kolor& kolor__, const c_wektor& w__, const double& promien__ ):
                        masa_( masa__ ), kolor_( kolor__ ), w_( w__ ), promien_( promien__ ){;};
       
       
       long double& masa(){ return masa_; }
       const long double& masa() const { return masa_; }
       
       c_kolor& kolor(){ return kolor_; }
       const c_kolor& kolor() const { return kolor_; }
       
       ///Metoda umo�liwiaj�ca przekazanie wektora po�o�enia do obiektu klasy vector<c_wektor>
       c_wektor wektor_polozenia(){ return w_/skala; }
       
       c_wektor& w(){ return w_; }
       const c_wektor& w() const { return w_; }
       
       c_wektor& v(){ return v_; }
       const c_wektor& v() const { return v_; }
       
       double& r(){ return promien_; }
       const double& r() const { return promien_; }
       
      };
      
      
//========================================================================================
//========================================================================================  




//========================================================================================
//========================================================================================  

//=============================== klasa obslugujaca w calosci efekt odbicia;] ============

///Klasa zawieraj�ca metody numeryczne i parametry opisuj�ce zjawisko odbicia
class zderzenia{      
      private:
        ///procentowe straty energii po zderzeniu
        double wsp_straty_energii; 
        ///zapamietanie odleglosci pomiedzy kulami                          
        double odleglosc; 
        c_wektor v2_2_;
        c_wektor v1_2_;
        
        c_wektor v1;
        c_wektor v2;
        
        c_wektor pol_ciala_1;
        c_wektor pol_ciala_2;
        
        
        c_wektor v1_n;
        c_wektor v1_p;
        
        c_wektor v2_n;
        c_wektor v2_p;
        
        c_wektor v2_n2;
        c_wektor v1_n2;
        ///Wektor normalny
        c_wektor n_norm;
        

        c_wektor normalizuj_wektor( const c_wektor& ); //wektor normalny akcji
               
        void przesun_na_zewnatrz( const materia_kulista&, const materia_kulista& );//przesuniecie kuli na zewnatrz innej w przypadku przenikniecia
                                                        //materia 1 przenika druga
                                                        
        void rzutuj_wektory_na_wektor_akcji(void);

                       
        
      public:
        
        zderzenia():wsp_straty_energii( 1.0 ), odleglosc( 0.0 ){;};
        zderzenia( const double& wsp_ ):wsp_straty_energii( wsp_ ), odleglosc( 0.0 ){;};
        
                                                                           /*!
                                                                            Oblicza, czy nast�pi�o zjawisko numerycznego przenikania si� sfer
                                                                            r1, r2, d(odleglosc) - 
                                                                            odleglosc jest raz obliczana dla
                                                                            danej pary obiektow dla zminimalizowania liczby obliczen
                                                                            i zwiekszenia szybkosci dzialania symulacji
                                                                            Obliczania jest w podpetli glownej metody obliczajacej
                                                                            przed wywolaniem nniejszej
                                                                            czyli odleglosc ta jest wykorzystywana do sprawdzania czy nastapilo zderzenie
                                                                            oraz do obliczania sily przyciagania grawitacyjnego cial
                                                                            */
        bool czy_zderzenie( const double&, const double&, const double& );
         
        /*!
        * Oblicza warto�ci pr�dko�ci po zderzeniu si� kul zgodnie
        * z Zasad� Zachowania Energii i P�du
        * a nast�pnie przekazuje warto�ci wektor�w pr�dko�ci i po�o�enia do zmiennych prywatnych
        */
        void obliczaj_po_zderzeniu(  const materia_kulista&, const materia_kulista&);
        
        
        const double& wspolczynnik_straty_energii()const { return wsp_straty_energii; }
        double& wspolczynnik_straty_energii(){ return wsp_straty_energii; }
        
        ///Wektor pr�dko�ci drugiego cia�a po zderzeniu z pierwszym
        const c_wektor& v2_2(void) const { return v2_2_;}//wyjscie
        ///Wektor pr�dko�ci pierwszego cia�a po zderzeniu z drugim
        const c_wektor& v1_2(void) const { return v1_2_;} //wyjscie
        ///Wektor po�o�enia cia�a 1. po umieszczeniu go na zewn�trz 2.
        const c_wektor& w1_2(void) const { return pol_ciala_1; }//wyjscie
        ///Wektor po�o�enia cia�a 2. po umieszczeniu go na zewn�trz 1.
        const c_wektor& w2_2(void) const { return pol_ciala_2; }//wyjscie
        
        //Obiekt jest raz inicjowany dlatego te� trzeba zerowac dla pewno�ci po kazdym odbiciu
        void zderzenia_zeruj_wektory(void); 
                                                                                    
        ~zderzenia(){
                     zderzenia_zeruj_wektory(); 
                     };
        
     };


///Normalizuje wektor
c_wektor zderzenia::normalizuj_wektor( const c_wektor& w )
{
   
   double dl=w.dlugosc();
   
   return c_wektor( w.x() / dl,  w.y() / dl,  w.z() / dl );     
}

bool zderzenia::czy_zderzenie( const double& r1, const double& r2, const double& d)
{
   odleglosc=d;  
   return (   d<=(r1+r2)  )? true : false;  
}

void zderzenia::obliczaj_po_zderzeniu(  const materia_kulista& mat1, const materia_kulista& mat2 )
{
  
 
     przesun_na_zewnatrz( mat1, mat2 ); 
     
     v1=mat1.v();
     v2=mat2.v();
     
     rzutuj_wektory_na_wektor_akcji();
    
     //=====oblicza zjawisko odbicia z zasady zachowania pedu i energii=========
     
 
     v2_n2= ( 2*v1_n + v2_n*( (mat2.masa()/mat1.masa()) - 1.0 ) )/ ( (mat2.masa()/mat1.masa()) + 1.0 );
     
     v1_n2= v1_n + (mat2.masa()/mat1.masa()) * ( v2_n - v2_n2 );
     
     
     //=========================================================================
     
     //====wyliczam wypadkowe skladowych prostopadlych i normalnych=============
     
     
     v2_2_ = v2_n2 + v2_p;
     
     v1_2_ = v1_n2 + v1_p;
     
     

                                                    
     //=========================================================================
     

     
}

void zderzenia::przesun_na_zewnatrz( const materia_kulista& mat1, const materia_kulista& mat2)
{

                     
   pol_ciala_2= mat1.w() + ( ( mat1.r() + mat2.r() )*(  mat2.w() - mat1.w() ) ) / odleglosc;

   pol_ciala_1= mat2.w() + ( ( mat1.r() + mat2.r() )*(  mat1.w() - mat2.w() ) ) / odleglosc;
    

}

void zderzenia::rzutuj_wektory_na_wektor_akcji(void)
{
    c_wektor n = pol_ciala_2 - pol_ciala_1;
    
    n.dlugosc_licz();
    
    n_norm = normalizuj_wektor( n );
    
   

    v1_n= n_norm * ( v1 % n_norm ) *wsp_straty_energii;
    v1_p= v1 - v1_n;
    

    
    v2_n= n_norm * ( v2 % n_norm ) *wsp_straty_energii;
    v2_p= v2 - v2_n;
    

}



void zderzenia::zderzenia_zeruj_wektory(void)
{
     
        odleglosc=0.0; //zapamietanie odleglosci pomiedzy kulami
        
         v2_2_.zeruj();
         v1_2_.zeruj();
        
         pol_ciala_1.zeruj();
         pol_ciala_2.zeruj();
        
         v1_n.zeruj();
         v1_p.zeruj();
        
         v2_n.zeruj();
         v2_p.zeruj();
        
         v2_n2.zeruj();
         v1_n2.zeruj();
         
         v1.zeruj();
         v2.zeruj();
         
         n_norm.zeruj();
     
}

//========================================================================================
//========================================================================================   

///Klasa odpowiedzialna za dok�adno�� liczenia oddzia�ywania grawitacyjnego mi�dzy jedn�, a drug� materi�
class grawitacja{
       ///Sta�a grawitacji[ N*m^2/kg^2 ]         
       const long double G; //[ N*m^2/kg^2 ] 
       
       c_wektor wylicz_skladowe_przyspieszenia( const long double& ,  const c_wektor & , const c_wektor& );
       /// Wektor przyspieszenia grawitacyjnego ciala 1.
       c_wektor a_1_;// wektor przysp ciala 1
       /// Wektor przyspieszenia grawitacyjnego 1
       c_wektor a_2_;// wektor przysp ciala 2
       /// Odleg�o�� mi�dzy cia�ami
       long double dlu;
       
      public:
        grawitacja():G( 6.6742867676767676767676767e-11 ), dlu(0.0){;};     
        
        void obliczaj_grawitacje( const materia_kulista&, const materia_kulista&, const long double&  );
        
        void grawitacja_zeruj_wektory(void)
        {
         a_1_.zeruj();
         a_2_.zeruj();  
        }
        
        const c_wektor& a_1()const{ return a_1_;}; 
        const c_wektor& a_2()const{ return a_2_;};         
             
        ~grawitacja(){;};
      };

void grawitacja::obliczaj_grawitacje( const materia_kulista& a, const materia_kulista& b, const long double& d )
{
 dlu= d;    
 long double f=G*a.masa()*b.masa() / (dlu*dlu);
 
 
 a_1_=wylicz_skladowe_przyspieszenia( f/a.masa(), b.w(), a.w() ) ; 
 a_2_=wylicz_skladowe_przyspieszenia( f/b.masa(), a.w(), b.w() ) ;  
 

 
}
///Wylicza sk�adowe przyspieszenia metod� podobie�stwa tr�jk�t�w, pomijaj�c kosztowne numerycznie obliczanie warto�ci sin i cos
c_wektor grawitacja::wylicz_skladowe_przyspieszenia( const long double& a,  const c_wektor & w1, const c_wektor& w2 )
{
    return  ( ( a*( w1-w2 ) )/dlu );
}

//========================================================================================
//========================================================================================        


//========================================================================================
//========================================================================================  
///Klasa zawieraj�ca metody i funkcje rysuj�ce wektor pr�dko�ci
class Cwektory_predkosci{
      bool rysuj;
      /// Wsp�czynnik szerokosci wektora wy�wietlanego
      double d;  // wsp. szerokosci wektora
      /// Wsp�czynnik powiekszenia wektora pr�dko�ci
      double powv; //wsp. powiekszenia wektora  
         
      public:
             Cwektory_predkosci():rysuj( false ), d( 10e6 ), powv( 20000 ){;};
             
             const bool& czy_rysuj_wektory_predkosci()const { return rysuj; }
             bool& czy_rysuj_wektory_predkosci(){ return rysuj; }
             
             const double& szerokosc()const { return d; }
             double& szerokosc(){ return d; }
             
             void rysuj_wektor( const c_wektor& wsp, const c_wektor& v );
      
      };

void Cwektory_predkosci::rysuj_wektor( const c_wektor& wsp, const c_wektor& v )
{
   glPushMatrix();   
                     glBegin(GL_QUADS);//g�ra
                        /*1*/   glColor4f(1.0, 0.0, 0.0, 1.0);      glVertex3f( (wsp.x() - d)/skala            , (wsp.y() - d)/skala            , (wsp.z()+d)/skala               );
                        /*2*/   glColor4f(1.0, 0.0, 0.0, 1.0);      glVertex3f( (wsp.x() + d)/skala            , (wsp.y() - d)/skala            , (wsp.z()+d) /skala              );
                        /*3*/   glColor4f(0.0, 1.0, 0.0, 1.0);      glVertex3f( (wsp.x() + d+ v.x()*powv )/skala, (wsp.y() + d+ v.y() *powv )/skala, ( (wsp.z()+d) + v.z()*powv)/skala );
                        /*4*/   glColor4f(0.0, 1.0, 0.0, 1.0);      glVertex3f( (wsp.x() - d+ v.x()*powv )/skala, (wsp.y() + d+ v.y() *powv )/skala, ( (wsp.z()+d) + v.z()*powv)/skala );
                     glEnd();
                     
                     glBegin(GL_QUADS);//d�
                        /*7*/   glColor4f(1.0, 0.0, 0.0, 1.0);      glVertex3f( (wsp.x() - d)/skala            , (wsp.y() - d)/skala            , (wsp.z() - d)/skala               );
                        /*8*/   glColor4f(0.0, 1.0, 0.0, 1.0);      glVertex3f( (wsp.x() - d+ v.x()*powv )/skala, (wsp.y() + d+ v.y()* powv )/skala, ( (wsp.z() - d) + v.z()*powv)/skala );
                        /*5*/   glColor4f(0.0, 1.0, 0.0, 1.0);      glVertex3f( (wsp.x() + d+ v.x()*powv )/skala, (wsp.y() + d+ v.y()* powv )/skala, ( (wsp.z() - d) + v.z()*powv)/skala );
                        /*6*/   glColor4f(1.0, 0.0, 0.0, 1.0);      glVertex3f( (wsp.x() + d)/skala            , (wsp.y() - d)/skala            , (wsp.z() - d) /skala              );
                     glEnd();     
   glPopMatrix();  
}


//========================================================================================
//========================================================================================  
///Klasa opisuj�ca i rysuj�ca p�aszczyzn� uk�adu odniesienia
class Csiatka{
      bool rysuj;
      void prostokat( const c_wektor&, const c_wektor&, const c_wektor&, const c_wektor& );
      public:
             Csiatka():rysuj( false ){;};
             
             const bool& czy_rysuj_siatke()const { return rysuj; }
             bool& czy_rysuj_siatke(){ return rysuj; }
             
             void rysuj_siatke();
      
      };

void Csiatka::prostokat( const c_wektor& a , const c_wektor& b, const c_wektor& c, const c_wektor& d)
{
        //1                4
        //
        //2                3  

    glPushMatrix();
          
   

 glBegin(GL_QUADS);//g�ra
 //glNormal3f(0.f, -1.f, 0.f);
   /*1*/   glColor4f(0.0, 1.0, 1.0, 1.0);      glVertex3f( a.x() , a.y(), a.z()  );
   /*2*/   glColor4f(1.0, 1.0, 1.0, 1.0);      glVertex3f( b.x() , b.y(), b.z()  );
                
   /*3*/   glColor4f(1.0, 1.0, 1.0, 1.0);      glVertex3f( c.x() , c.y()  , c.z() );
   /*4*/   glColor4f(0.0, 1.0, 1.0, 1.0);      glVertex3f( d.x() , d.y()  , d.z()  );
// glEnd();

 //glBegin(GL_QUADS);//d�
// glNormal3f(0.f, -1.f, 0.f);
    /*7*/   glColor4f(1.0, 0.0, 0.0, 1.0);      glVertex3f( a.x() , a.y()  , a.z()  ); //1
    /*8*/   glColor4f(0.0, 1.0, 0.0, 1.0);      glVertex3f( d.x() , d.y()  ,  d.z() );  //4
    /*5*/   glColor4f(0.0, 1.0, 0.0, 1.0);      glVertex3f( c.x()  , c.y()   ,  c.z()  ); //3
    /*6*/   glColor4f(1.0, 0.0, 0.0, 1.0);      glVertex3f( b.x() , b.y(),    b.z()  ); //2
                
  glEnd();
      	  
 glPopMatrix(); 
     
}    

  
void Csiatka::rysuj_siatke()
{
  double  itmp=50*10e7/skala ;
  double  skok=itmp/50.0; 
  double  szer=skok/20.0;
  
  for(double i=-itmp; i<itmp; i+=skok)
   {   
    c_wektor a1(i       , -itmp, 0.0);//1
    c_wektor b1(i+ szer , -itmp  , 0.0);//2
    c_wektor c1(i+ szer , itmp , 0.0);//3
    c_wektor d1(i       , itmp , 0.0);//4
        
    prostokat(a1, b1, c1, d1);
   }


  for(double i=-itmp; i<itmp; i+=skok)
   {   
    
    c_wektor a1(-itmp, i, 0.0);//1
    c_wektor b1(itmp , i, 0.0);//2
    c_wektor c1(itmp , i+ szer, 0.0);//3
    c_wektor d1(-itmp, i+ szer, 0.0);//4
        
    prostokat(a1, b1, c1, d1);
   }
  	  	      
     
}
      
//========================================================================================
//========================================================================================   

/*!
* Klasa przechowuj�ca parametry po�o�enia kamery
* oraz wsp�czynniki obrotu kamery wok� lokalnie utworzonego uk�adu wsp��dnych
*/
class Ckamera{
      ///Sk�adowa po�o�enia x
      double translatedx;
      ///Sk�adowa po�o�enia y
      double translatedy;
      ///Sk�adowa po�o�enia z
      double translatedz;
      ///K�t obrotu wok� lokalnej osi x
      double rotatex;
      ///K�t obrotu wok� lokalnej osi y
      double rotatey;
      
      public:
             Ckamera():translatedx(0.0), translatedy(0.0), translatedz(0.0), rotatex(0.0), rotatey(0.0){;};
       
             const double& translated_x()const { return translatedx; }
             double& translated_x(){ return translatedx; }
             
             const double& translated_y()const { return translatedy; }
             double& translated_y(){ return translatedy; }
             
             const double& translated_z()const { return translatedz; }      
             double& translated_z(){ return translatedz; }
             
             c_wektor wektor_polozenia() { return c_wektor( translatedx, translatedy, translatedz ); };
             
             const double& rotate_x()const { return rotatex; }
             double& rotate_x(){ return rotatex; }

             const double& rotate_y()const { return rotatey; }
             double& rotate_y(){ return rotatey; }
                          
      };



//========================================================================================
//======================================================================================== 
 
///Klasa odpowiedzialna za parametry rysowanej sfery 
class Csfera_opcje{
             ///Liczba "r�wnole�nik�w" sfery
             int slices_;
             ///Liczba "po�udnik�w" sfery
             int stacks_;
      
      public:
             Csfera_opcje():slices_(20), stacks_(20){;};
             
                    
             const int& slices()const { return slices_; }
             int& slices(){ return slices_; }
             
             const int& stacks()const { return stacks_; }
             int&  stacks(){ return stacks_; }
      
      }; 
 
//========================================================================================
//======================================================================================== 
///Klasa przechowuj�ca tablic� obiekt�w
class zbior_obiektow{
      materia_kulista* obiekty_;
      int n_obiektow_;
      public:
        zbior_obiektow():n_obiektow_(0){;};
        
        void inicjuj(const int& n)
        {
         obiekty_=new materia_kulista[n];
         n_obiektow_=n;     
        }
             
        const int& n_obiektow()const { return n_obiektow_;};
        int& n_obiektow() { return n_obiektow_;};
        
        const materia_kulista& obiekt( const int& i )const { return obiekty_[i];};
        materia_kulista& obiekt( const int& i ) { return obiekty_[i];};
        
        ~zbior_obiektow(){ delete [] obiekty_; };
        
      };   

//========================================================================================
//======================================================================================== 
///Klasa zawieraj�ca metody numeryczne, obliczaj�ce rozk�ad nat�enia pola grawitacyjnego 
class Cpole_ekwipotencjalne{
      bool rysuj;
      c_wektor srodek_;
      double dx_;
      double dy_;
      int nx_;
      int ny_;
      long double ekwipotencjal_z_w_punkcie( const c_wektor&,  const zbior_obiektow&);  
      long double G;
      
      ///Wsp�czynnik skali nat�enia grawitacji i koloru
      double wspolczynnik_mnozenia_;

      
      public:
             Cpole_ekwipotencjalne():wspolczynnik_mnozenia_(1.0),  rysuj( false ), dx_( 0.0 ), dy_( 0.0 ), nx_(0), ny_(0), G(6.6742867676767676767676767e-11)
             {

             };
      
             ///�rodek badanego obszaru
             const c_wektor& pole_ekwipotencjalne_srodek()const { return srodek_;};
             c_wektor& pole_ekwipotencjalne_srodek() { return srodek_;};
                          
             const bool& czy_rysuj_pole_ekwipotencjalne()const { return rysuj; };
             bool& czy_rysuj_pole_ekwipotencjalne(){ return rysuj; };
             ///Szeroko�� dx podpola
             const double& pole_dx()const {return dx_;};
             double& pole_dx(){ return dx_;};
             ///Wysoko�� dy podpola
             const double& pole_dy()const {return dy_;};
             double& pole_dy(){ return dy_;};
             ///Liczba podp�l wzd�u� osi x
             const int& pole_nx()const {return nx_;};  
             int& pole_nx(){ return nx_;};
             ///Liczba podp�l wzd�u� osi y             
             const int& pole_ny()const {return ny_;};                        
             int& pole_ny(){ return ny_;};              
             ///Wsp�czynnik mno�enia, nasilenia barwy             
             const double& wspolczynnik_mnozenia()const {return wspolczynnik_mnozenia_;};  
             double& wspolczynnik_mnozenia(){ return wspolczynnik_mnozenia_;};
 
             ///Odpowiada za prawid�owe odwzorowanie kolor�w dla danego pola grawitacyjnego
             void skala_koloru( const double& );
             
             void rysuj_pole_ekwipotencjalne(  const zbior_obiektow& ); 
      };
      


void Cpole_ekwipotencjalne::skala_koloru( const double& k )
{
for(int i=0; i<200; i++) 
 if( k>=i*10 && k<10*(i+1) )
  glColor4f(  (i - 100.0 ) / 100.0 , (i - 50.0 ) / 50.0, ( i - 2.0 ) / 48.0, (i) / 200.0); 
}

long double Cpole_ekwipotencjalne::ekwipotencjal_z_w_punkcie( const c_wektor& p,  const zbior_obiektow&  z) //const vector<materia_kulista>&
{
   long double pot=0.0;    
   long double tmp=0.0 ;   
   long double dlu_kw=0.0;
   c_wektor    wek_tmp;
      
   for(int i=0; i<z.n_obiektow(); i++)
   {
    dlu_kw=( z.obiekt(i).w().x() - skala * p.x() )*( z.obiekt(i).w().x() - skala*p.x() ) +  ( z.obiekt(i).w().y() - skala* p.y() )*( z.obiekt(i).w().y() - skala * p.y() ) + ( z.obiekt(i).w().z() - skala* p.z() )*( z.obiekt(i).w().z() - skala * p.z() );       
    tmp = G*z.obiekt(i).masa()/ (  dlu_kw ) ;   
        
    wek_tmp = wek_tmp + tmp*( z.obiekt(i).w()-p   )/sqrt(dlu_kw);
   }   
   
   wek_tmp.dlugosc_licz();
   
   pot=wek_tmp.dlugosc();
   
   
    return     pot   * wspolczynnik_mnozenia_ ;   
} 

     
void Cpole_ekwipotencjalne:: rysuj_pole_ekwipotencjalne(  const zbior_obiektow&  zbior ) //const vector<materia_kulista>&  zbior
{
glPushMatrix(); 
     
 c_wektor temp( 0.0, 0.0, srodek_.z() );
 long double ekw_w_p=0.0;
 double tmp=0.0;
 double max_x= srodek_.x() + 0.5*nx_*dx_;
 double max_y= srodek_.y() + 0.5*ny_*dy_;
 double min_x= srodek_.x() - 0.5*nx_*dx_;
 double min_y= srodek_.y() - 0.5*ny_*dy_; 
 // pole ekwipotencjalne rysowane jest od "gory"
         
         for( double i=min_x; i<max_x; i+=dx_ )
              {
              for( double j=min_y; j<max_y; j+=dy_ )
                   {
                      glBegin(GL_QUADS);//g�ra
                      
                                            temp.x()=i-0.5*dx_;
                                            temp.y()=j-0.5*dy_;
                                            ekw_w_p=ekwipotencjal_z_w_punkcie( temp, zbior );
                                            tmp=( ( ekw_w_p )   );
                                            skala_koloru(tmp);                                                                    
                      /*1*/       glVertex3f( i-0.5*dx_ , j-0.5*dy_, srodek_.z()  );
                      
                                            temp.x()=i+0.5*dx_;
                                            temp.y()=j-0.5*dy_;
                                           // temp.z=z;
                                            ekw_w_p=ekwipotencjal_z_w_punkcie( temp, zbior );
                                            tmp=( ( ekw_w_p )  );   
                                            skala_koloru(tmp);
                     /*2*/        glVertex3f( i+0.5*dx_ , j-0.5*dy_, srodek_.z()  );
                     
                                            temp.x()=i+0.5*dx_;
                                            temp.y()=j+0.5*dy_;
                                            //temp.z=z;
                                            ekw_w_p=ekwipotencjal_z_w_punkcie( temp, zbior );
                                            tmp=( ( ekw_w_p )  ); 
                                            skala_koloru(tmp);                   
                     /*3*/         glVertex3f( i+0.5*dx_ , j+0.5*dy_  , srodek_.z() );
                     
                                            temp.x()=i-0.5*dx_;
                                            temp.y()=j+0.5*dy_;
                                            //temp.z=z; 
                                            ekw_w_p=ekwipotencjal_z_w_punkcie( temp, zbior );
                                            tmp=( ( ekw_w_p )  );   
                                            skala_koloru(tmp);                                                             
                      /*4*/        glVertex3f( i-0.5*dx_ , j+0.5*dy_  , srodek_.z()  );
                      glEnd(); 
                  }
              }
glPopMatrix();  
};     



//========================================================================================
//======================================================================================== 

//========================================================================================
//======================================================================================== 

//========================================================================================
//======================================================================================== 

//========================================================================================
//======================================================================================== 
///Klasa przechowuj�ca obiekty klasy vector<c_wektor> wraz z parametrami pr�bkowania
class Ctrajektoria
{
  vector<c_wektor> wezly_;
  c_kolor kolor_;
  int     co_ile_krokow_;
  ///Przechowuje ile krok�w pomini�to przy czekaniu na warto�� co_ile_krokow
  int licznik_krokow_;
  
  public:
     Ctrajektoria():co_ile_krokow_(1), licznik_krokow_(0), kolor_( 1.0, 1.0, 1.0 ){;};        
     Ctrajektoria( const int& );
     void dodaj_wezel( const c_wektor& );
     void rysuj( void );
     
     const int& co_ile_krokow() const{ return co_ile_krokow_; };
     int& co_ile_krokow(){ return co_ile_krokow_; };

     const int& licznik_krokow() const{ return licznik_krokow_; };
     int& licznik_krokow(){ return licznik_krokow_; };

     const c_kolor& kolor() const{ return kolor_; };
     c_kolor& kolor(){ return kolor_; };
     
     long liczba_wezlow(){ return wezly_.size(); };
     
     ~Ctrajektoria()
     {
     wezly_.clear();
     };
};

Ctrajektoria::Ctrajektoria( const int& i )
{
 c_wektor tmp;
 wezly_.resize( i, tmp );
}

void Ctrajektoria::dodaj_wezel( const c_wektor& w )
{
  wezly_.resize( wezly_.size()+1, w );   
}

void Ctrajektoria::rysuj( void )
{
   glPushMatrix(); 
                   if(wezly_.size() >2 )for(int i=0; i<wezly_.size() -1 ; i++)
                   {  
                     glBegin(GL_LINES);
                                      glColor3f( kolor_.r(), kolor_.g(), kolor_.b() );
                                      glVertex3f( wezly_[i].x(), wezly_[i].y(), wezly_[i].z() );
                                      glVertex3f( wezly_[i+1].x(), wezly_[i+1].y(), wezly_[i+1].z() );
                     glEnd(); 
                   }      
   glPopMatrix();       
}


//========================================================================================
//======================================================================================== 


///G��wna klasa symulacji
class symulacja:   public grawitacja, public zderzenia{
        
       //vector<materia_kulista> obiekty_; //zbior obiektow
       
       zbior_obiektow obiekty;
       
       
       vector<Ctrajektoria>  trajektoria_; //demo;]
       Cpole_ekwipotencjalne pole_;
       Cwektory_predkosci    wektory_;
       Csfera_opcje          sfera_;
       Ckamera               kamera_;
       Csiatka               siatka_;

       
	   static symulacja sym;

        bool dodatkowe_informacje_; 
        long klatek_na_sekunde_;       
        double czas_100_klatek_;
        int i_100_klatek_;
        bool klawisze_help_;
        bool pauza_;                
        double t_;
        
        ///Liczba krokow obliczanych przy wywolaniu obliczaj()                
        int l_krokow_podrzednych_;
        ///Krok czasowy
        double dt_;
        ///Liczba krokow obliczonych dotychczas (liczba wywolan funkcji obliczaj()
        long l_krokow_glownych_;   
         
        ///Nazwa aktualnej sesji
        string sesja_;
        /// Tymczasowy wektor odleglosci
        c_wektor odl_; // tymczasowy wektor odleglosci
         
      public:
             symulacja():  l_krokow_podrzednych_(0),  l_krokow_glownych_(0), t_(0.0), i_100_klatek_(0), czas_100_klatek_(0.0)
             {
              pauza_=true;
              klawisze_help_=true;
             };
             ///Czyta plik sesyjny a nastepnie przechodzi do klatki zdefiniowanej przez uzytkownika w pliku klatki
             void czytaj_z_plikow( const string& , const string& ); 
             void czytaj_plik_sesji( const string& );
             void zapisz_krok_do_pliku(void);
              
              ///Generuje przypadkowe warto�ci wektoru po�o�enia i masy cia�a 
              void generuj( void );
              
              ///Rysuje funkcje klawiszy
              void klawisze_help(void);
              ///Wypisuje dodatkowe informacje na temat symulacji
              void dodatkowe_informacje(void);
              
              
              double string_to_double(const string& );
              int    string_to_int(const string& );
              string int_to_string(const long& );
              
              ///Wy�wietla tekst na ekranie w okre�lonej pozycji w przestrzeni
              void glutPisz( const float& , const float& , void *, const string& , const float&);

              ///Rysuje scene            
              static void rysuj(void);
              /// Obs�uga klawiaturki
              static void klawisze(unsigned char , int , int );
              /// Obs�uga klawiszy mychy               
              static void MouseButton (int , int , int , int );
              /// Obs�uga ruch�w myszy
              static void MouseMotion (int , int );
              /// Oblicza po�o�enie cia� w danym kroku czasowym
              void obliczaj(void);
              void zapisz_plik_sesji();
              void inicjuj_sesje();
              
      
      //metoda w klasie ktora zwraca referencje do tej klasy 
        static symulacja& referencja(){ return sym; }        
      
        
        const Cpole_ekwipotencjalne& pole()const { return pole_;};
        Cpole_ekwipotencjalne& pole() { return pole_;};
        
        const Cwektory_predkosci& wektory()const { return wektory_;};
        Cwektory_predkosci& wektory() { return wektory_;};

        const Csfera_opcje& sfera()const { return sfera_;};
        Csfera_opcje& sfera() { return sfera_;};

        const Ckamera& kamera()const { return kamera_;};
        Ckamera& kamera() { return kamera_;};              

        const Csiatka& siatka()const { return siatka_;};
        Csiatka& siatka() { return siatka_;};

        const bool& pauza()const { return pauza_;};
        bool& pauza() { return pauza_;};              

        const int& l_krokow_podrzednych()const{ return l_krokow_podrzednych_; };//liczba krokow obliczanych przy wywolaniu obliczaj()
        int& l_krokow_podrzednych(){ return l_krokow_podrzednych_; };
             
        const double& dt()const{ return dt_; };
        double& dt(){ return dt_; };       
                       
        const double& t()const{ return t_;};
        double& t(){ return t_;};
        
        const long& l_krokow_glownych()const{ l_krokow_glownych_;};   //liczba krokow obliczonych dotychczas (liczba wywolan funkcji obliczaj()
        long& l_krokow_glownych(){ l_krokow_glownych_;}; 
        
        const string& sesja() const { return sesja_; }
        string& sesja(){ return sesja_; }
             
         
             
 
            
             ~symulacja()
             {
                       // obiekty_.clear();
                      
             };
      };

symulacja symulacja::sym;

void symulacja::glutPisz( const float& x, const float& y , void * font, const string& txt, const float& scale)
{
 int i;
  int len=txt.size();
 float tmp_y=y; 
 for (i=0; i<len; i++)
 {
  glRasterPos2f(x, tmp_y);
  glutBitmapCharacter(font, txt[i]);
  tmp_y-=glutBitmapWidth(font, txt[i])*scale/600;
 }    
     
}

string symulacja::int_to_string(const long& i)
{
  char buffer [50];
  string tmp;
  
 itoa (i,buffer,10);
  tmp=buffer;

  return tmp; 
 
}

void symulacja::dodatkowe_informacje(void)
{
     double wys=0.1;
     double dlug=1.0;
     
glPushMatrix();  
   
   glTranslated(kamera_.translated_x() , kamera_.translated_y(), kamera_.translated_z()+1.0 );  
   
                      glBegin(GL_QUADS);
                        glColor3d(1.0, 0.0, 0.0);      glVertex3f( -0.5*wys -0.35f, -0.5*dlug, -0.01  );
                        glColor3d(1.0, 1.0, 0.0);      glVertex3f( 0.5*wys -0.25f, -0.5*dlug, -0.01  );
                        glColor3d(1.0, 1.0, 0.0);      glVertex3f( 0.5*wys -0.25f, 0.5*dlug  , -0.01 );
                        glColor3d(1.0, 0.0, 0.0);      glVertex3f( -0.5*wys -0.35f, 0.5*dlug  , -0.01  );
                      glEnd(); 
  
          
   string tmp; 

  glColor3d( 1.0f, 0.0f, 0.0f );    
   tmp=int_to_string( l_krokow_glownych_ );
   tmp+=" [krokow glownych]";
   glutPisz(-0.23f, 0.3f, GLUT_BITMAP_9_BY_15, tmp, 1.0);
   tmp=" ";
  
  glColor3d( 1.0f, 1.0f, 1.0f );
   tmp=int_to_string( (int)(l_krokow_glownych_*dt_) );
   tmp+=" [sekund swiata symulacji]";
   glutPisz(-0.25f, 0.3f, GLUT_BITMAP_9_BY_15, tmp, 1.0);
   tmp=" ";
   
   tmp=int_to_string( (int)(t_) );
   tmp+=" [sekund symulacji]";
   glutPisz(-0.27f, 0.3f, GLUT_BITMAP_9_BY_15, tmp, 1.0);
   tmp=" ";

  glColor3d( 0.0f, 0.0f, 1.0f );   
   tmp=int_to_string( klatek_na_sekunde_ );
   tmp+=" [k/s]";
   glutPisz(-0.29f, 0.3f, GLUT_BITMAP_9_BY_15, tmp, 1.0);
   tmp=" ";   
   
   long suma=0;
   for( int i=0; i<trajektoria_.size(); i++) suma+=trajektoria_[i].liczba_wezlow();
  glColor3d( 0.0f, 0.0f, 1.0f );   
   tmp=int_to_string( suma );
   tmp+=" wezlow wszystkich trajektorii;]";
   glutPisz(-0.31f, 0.3f, GLUT_BITMAP_9_BY_15, tmp, 1.0);
   tmp=" "; 

  glColor3d( 1.0f, 1.0f, 1.0f );   
   glutPisz(-0.34f, 0.3f, GLUT_BITMAP_9_BY_15, " Skala - 1:10e8 ( kwadrat siatki ma 5000km )", 1.0);
   tmp=" "; 
     
glPopMatrix();     
}

void symulacja::klawisze_help(void)
{
glPushMatrix();     
     
   double szer=0.6, dlug=0.6;  
   
   glTranslated(sym.kamera_.translated_x() , sym.kamera_.translated_y(), kamera_.translated_z()+1.1 ); 
  /*   
                      glBegin(GL_QUADS);//g�ra
                        glColor4f(1.0, 0.0, 0.0, 0.5);      glVertex3f( -0.5*szer , -0.5*dlug, 0.0  );
                        glColor4f(0.0, 1.0, 0.0, 0.5);      glVertex3f( 0.5*szer , -0.5*dlug, 0.0  );
                        glColor4f(1.0, 1.0, 0.0, 0.5);      glVertex3f( 0.5*szer , 0.5*dlug  , 0.0 );
                        glColor4f(0.0, 0.0, 1.0, 0.5);      glVertex3f( -0.5*szer , 0.5*dlug  , 0.0  );
                      glEnd(); 
   */
                           
   glTranslated(0.0f , 0.0f, -0.2 );  
   glColor3d( 1.0f, 1.0f, 1.0f ); 

 for(int i=0; i<31; i++)
 {
  glutPisz(1.0f - i*0.02f , 0.3f, GLUT_BITMAP_9_BY_15, help_klawiszy(i), 1.0);  
 } 
    
//===============================================================================================



//==========================================================================================
glPopMatrix();  
}

void symulacja::czytaj_plik_sesji( const string& nazwa_sesji )
{
 ifstream plik;
 plik.open( ( nazwa_sesji+".PRO" ).c_str() );
 
 sesja_=nazwa_sesji;
 
 string      stmp;
 bool        btmp;
 double      dtmp;
 int         itmp;
 long        ltmp;
 long double ldtmp;
 
 if( !plik ){;}
      else{
         for(int i=0; i<14; i++)plik>>stmp;
         
         plik>>btmp;
           siatka_.czy_rysuj_siatke()=btmp;
         plik>>stmp;
         plik>>btmp;
           pole_.czy_rysuj_pole_ekwipotencjalne()=btmp;
         plik>>stmp;
         plik>>dtmp;
           kamera_.translated_x()=dtmp;
         plik>>dtmp;
           kamera_.translated_y()=dtmp;
         plik>>dtmp;
           kamera_.translated_z()=dtmp;
         plik>>stmp;
         plik>>dtmp;
           pole_.pole_ekwipotencjalne_srodek().x()=dtmp;
         plik>>dtmp;
           pole_.pole_ekwipotencjalne_srodek().y()=dtmp;
         plik>>dtmp;
           pole_.pole_ekwipotencjalne_srodek().z()=dtmp;
                   
          plik>>stmp;

          plik>>itmp;
            pole_.pole_nx()=itmp;
          plik>>itmp;
            pole_.pole_ny()=itmp;
          plik>>stmp;
          plik>>dtmp;
            pole_.pole_dx()=dtmp;
          plik>>dtmp;
            pole_.pole_dy()=dtmp;
          plik>>stmp;
          plik>>dtmp;
            dt_=dtmp;
          plik>>stmp;
          plik>>itmp;
            l_krokow_podrzednych_=itmp;
          plik>>stmp;
          plik>>itmp;
             obiekty.inicjuj(itmp);
               Ctrajektoria tmp;    //demo;]
               trajektoria_.resize( itmp, tmp ); //demo;]
               
             //materia_kulista tmp;
             //obiekty_.resize(itmp, tmp);
          plik>>stmp;
          plik>>dtmp;
             wspolczynnik_straty_energii()=dtmp;
          plik>>stmp;
          plik>>dtmp;
             pole_.wspolczynnik_mnozenia()=dtmp;

            
             
       
        plik.close();  
      }
      
      
     
}

double symulacja::string_to_double(const string& czary_mary)
{
  ifstream plik_input;
  ofstream plik_output;     
  double tmp=0.0;     

  plik_output.open("plik_konwersji.tmp");
  plik_output<<czary_mary;
  plik_output.close();
  
  plik_input.open("plik_konwersji.tmp");
  plik_input>>tmp;
  plik_input.close();

  return tmp;       
}

int    symulacja::string_to_int(const string& czary_mary)
{
  ifstream plik_input;
  ofstream plik_output;     
  int tmp=0;     

  plik_output.open("plik_konwersji.tmp");
  plik_output<<czary_mary;
  plik_output.close();
  
  plik_input.open("plik_konwersji.tmp");
  plik_input>>tmp;
  plik_input.close();
       
    return tmp;
}
              
          
void symulacja::generuj(void)
{
 
 for(int i=0; i< obiekty.n_obiektow(); i++)
 {
         
  obiekty.obiekt(i).masa()=( rand()% ( (int)( (2*10e4) ) ) )* 10e20 + 10e6;
  obiekty.obiekt(i).w().x()=-3000000.0+((rand() % 300000)*10000+ 3);
  obiekty.obiekt(i).w().y()=-3000000.0+((rand() % 300000)*10000+ 3);
  obiekty.obiekt(i).w().z()=-3000000.0+((rand() % 300000)*10000+ 3);  

  obiekty.obiekt(i).r()=10000000;

   cout<<endl<<"losuje "<<i<<endl;



  obiekty.obiekt(i).kolor().r()=1;
  obiekty.obiekt(i).kolor().g()=1;
  obiekty.obiekt(i).kolor().b()=1;  
  
 }
 
               Ctrajektoria tmp;    //demo;]
               trajektoria_.resize( obiekty.n_obiektow(), tmp ); //demo;]
     
     
}

void symulacja::inicjuj_sesje()
{
     
     
}

void symulacja::czytaj_z_plikow( const string& nazwa_sesji, const string& nazwa_klatki)
{
 ifstream plik;
 plik.open( ( nazwa_sesji+".PRO" ).c_str() );
 
 sesja_=nazwa_sesji;
 
 string      stmp;
 bool        btmp;
 double      dtmp;
 int         itmp;
 long        ltmp;
 long double ldtmp;
 
 if( !plik ){;}
      else{
         for(int i=0; i<14; i++)plik>>stmp;
         
         plik>>btmp;
           siatka_.czy_rysuj_siatke()=btmp;
         plik>>stmp;
         plik>>btmp;
           pole_.czy_rysuj_pole_ekwipotencjalne()=btmp;
         plik>>stmp;
         plik>>dtmp;
           kamera_.translated_x()=dtmp;
         plik>>dtmp;
           kamera_.translated_y()=dtmp;
         plik>>dtmp;
           kamera_.translated_z()=dtmp;
         plik>>stmp;
         plik>>dtmp;
           pole_.pole_ekwipotencjalne_srodek().x()=dtmp;
         plik>>dtmp;
           pole_.pole_ekwipotencjalne_srodek().y()=dtmp;
         plik>>dtmp;
           pole_.pole_ekwipotencjalne_srodek().z()=dtmp;
                   
          plik>>stmp;

          plik>>itmp;
            pole_.pole_nx()=itmp;
          plik>>itmp;
            pole_.pole_ny()=itmp;
          plik>>stmp;
          plik>>dtmp;
            pole_.pole_dx()=dtmp;
          plik>>dtmp;
            pole_.pole_dy()=dtmp;
          plik>>stmp;
          plik>>dtmp;
            dt_=dtmp;
          plik>>stmp;
          plik>>itmp;
            l_krokow_podrzednych_=itmp;
          plik>>stmp;
          plik>>itmp;
             //materia_kulista tmp;
             //obiekty_.resize(itmp, tmp);
             obiekty.inicjuj(itmp);
               Ctrajektoria tmp;    //demo;]
               trajektoria_.resize( itmp, tmp ); //demo;]
             
          plik>>stmp;
          plik>>dtmp;
             wspolczynnik_straty_energii()=dtmp;
          plik>>stmp;
          plik>>dtmp;
             pole_.wspolczynnik_mnozenia()=dtmp;   

          
        plik.close();  
      }
      
 
 ifstream plik2;
 plik2.open( ( nazwa_klatki + ".DAT" ).c_str() );
 
 if( !plik2 ){;}
 else
 {
     
              plik2>>stmp;
              plik2>>stmp;
              plik2>>stmp;
              plik2>>stmp;
              
              plik2>>dtmp;
                t_=dtmp;
              plik2>>stmp;
              plik2>>ltmp;
                l_krokow_glownych_=ltmp;
              plik2>>stmp;

                
              for(int i=0; i<obiekty.n_obiektow(); i++)              
                      {
                           plik2>>stmp;
                           
                           
                           plik2>>obiekty.obiekt(i).w().x();
                           plik2>>obiekty.obiekt(i).w().y();
                           plik2>>obiekty.obiekt(i).w().z(); 
                                                     
                           plik2>>obiekty.obiekt(i).v().x();
                           plik2>>obiekty.obiekt(i).v().y();
                           plik2>>obiekty.obiekt(i).v().z(); 

                           plik2>>obiekty.obiekt(i).masa();
                           plik2>>obiekty.obiekt(i).r();

                           plik2>>obiekty.obiekt(i).kolor().r();
                           plik2>>obiekty.obiekt(i).kolor().g();
                           plik2>>obiekty.obiekt(i).kolor().b(); 
                           trajektoria_[i].kolor()=obiekty.obiekt(i).kolor();
              
                       }       
     
     
     
     
     
   plik2.close();  
 }   
     
     
}



void symulacja::MouseButton (int button, int state, int x, int y)
{
  
    if (button == GLUT_LEFT_BUTTON)
    {
 
        button_state = state;

        if (state == GLUT_DOWN)
        {
            button_x = x;
            button_y = y;
        }
    }
}

void symulacja::MouseMotion (int x, int y)
{
    
    if (button_state == GLUT_DOWN)
    {
        sym.kamera_.rotate_y() += 3 *1.0/glutGet (GLUT_WINDOW_WIDTH) * (x - button_x); 
        button_x = x;
        sym.kamera_.rotate_x() -= 3 *1.0/glutGet (GLUT_WINDOW_HEIGHT) * (button_y - y); 
        button_y = y;
        glutPostRedisplay ();
    }
}


void symulacja::obliczaj(void)
{
   int i=1;
          
	        for(int i1=0; i1<obiekty.n_obiektow()-1; i1++)
		            {
                          
		             for(int i2=i; i2<obiekty.n_obiektow() ; i2++)
                             {
                               
                               
                                   
                               odl_.wprowadz( obiekty.obiekt(i1).w().x() - obiekty.obiekt(i2).w().x(), obiekty.obiekt(i1).w().y() - obiekty.obiekt(i2).w().y(), obiekty.obiekt(i1).w().z() - obiekty.obiekt(i2).w().z() );
                               odl_.dlugosc_licz();
                               
                               if( czy_zderzenie( obiekty.obiekt(i1).r(), obiekty.obiekt(i2).r(), odl_.dlugosc() ) )
                                                  {
                                                    obliczaj_po_zderzeniu( obiekty.obiekt(i1), obiekty.obiekt(i2) );
                                                    
                                                    obiekty.obiekt(i1).v()=v1_2();
                                                    obiekty.obiekt(i2).v()=v2_2();
                                                    
                                                    obiekty.obiekt(i1).w()=w1_2();
                                                    obiekty.obiekt(i2).w()=w2_2();
                                                    
                                                    //p<<" v1_2() "<< v1_2().x()<<" "<<v1_2().y()<<" "<<v1_2().z()<<endl;
                                                    //p<<" v2_2() "<< v2_2().x()<<" "<<v2_2().y()<<" "<< v2_2().z()<<endl;
                                                    
                                                    //p<<" w1_2() "<< w1_2().x()<<" "<<w1_2().y()<<" "<<w1_2().z()<<endl;
                                                    //p<<" w2_2() "<< w2_2().x()<<" "<<w2_2().y()<<" "<<w2_2().z()<<endl;
                                                            
                                                             
                                                  }  
                                                  
                               obiekty.obiekt(i1).w()= obiekty.obiekt(i1).w() + 0.5* obiekty.obiekt(i1).v() * dt_;
                               obiekty.obiekt(i2).w()= obiekty.obiekt(i2).w() + 0.5* obiekty.obiekt(i2).v() * dt_;   
                               
                               obliczaj_grawitacje( obiekty.obiekt(i1), obiekty.obiekt(i2), odl_.dlugosc() );
                                                              
                               obiekty.obiekt(i1).v()= obiekty.obiekt(i1).v() + a_1() * dt_;                                                 
                               obiekty.obiekt(i2).v()= obiekty.obiekt(i2).v() + a_2() * dt_;
                              
                               
                               grawitacja_zeruj_wektory();                                   
                               zderzenia_zeruj_wektory();    
                               odl_.zeruj();    
                             }
                             
                     i++;
                    }    
     
}

void symulacja::zapisz_plik_sesji()
{
     
 ofstream plik;
 
 plik.open( ( sesja_+".PRO" ).c_str() );
 
 if( !plik ){;}
     else{
          plik<<"[NAZWA_SESJI]"<<endl;
          plik<<sesja_<<endl;
          plik<<endl;
          plik<<"[PROJEKT]"<<endl;
          plik<<"IMPLEMENTACJA_PODSTAWOWYCH_ZJAWISK_PRZYRODY"<<endl;
          plik<<endl;
          plik<<"[PROGRAMISTA]"<<endl;
          plik<<"RAFAL_STANCZUK_ROK.III._SEM.V"<<endl;
          plik<<endl;
          plik<<"[KOMPILACJA]"<<endl;
          plik<<__FILE__<<" "<<__DATE__<<"  "<<__TIME__<<endl;
          plik<<endl;
          plik<<"[__________OPCJE_SRODOWISKA__________]"<<endl;
          plik<<endl;
          plik<<"[RYSUJ_SIATKE]"<<endl;
          plik<<siatka_.czy_rysuj_siatke()<<endl;
          plik<<endl;
          plik<<"[RYSUJ_POLE_EKWIPOTENCJALNE]"<<endl;
          plik<<pole_.czy_rysuj_pole_ekwipotencjalne()<<endl;
          plik<<endl;          
          plik<<"[WSPOLZEDNE_OBSERWATORA]"<<endl;
          plik<<kamera_.translated_x()<<" "<<kamera_.translated_y()<<" "<<kamera_.translated_z()<<endl;
          plik<<endl;
          plik<<"[WSPOLZEDNE_SRODKA_POLA_EKWIPOTENCJALNEGO]"<<endl;
          plik<<pole_.pole_ekwipotencjalne_srodek().x()<<" "<<
                pole_.pole_ekwipotencjalne_srodek().y()<<" "<<
                pole_.pole_ekwipotencjalne_srodek().z()<<endl;
          plik<<endl;
          plik<<"[LICZBA_PRZEDZIALOW_POLA]"<<endl;
          plik<<pole_.pole_nx()<<" "<<pole_.pole_ny()<<endl;
          plik<<endl;
          plik<<"[SZEROKOSC_DX_DY_PRZEDZIAL_POLA]"<<endl;
          plik<<pole_.pole_dx()<<" "<<pole_.pole_dy()<<endl;
          plik<<endl;
          plik<<"[DT]"<<endl;
          plik<<dt_<<endl;
          plik<<endl;
          plik<<"[LICZBA_PODKROKOW_DT_OBLICZANYCH_W_JEDNYM_KROKU]"<<endl;
          plik<<l_krokow_podrzednych_<<endl;
          plik<<endl;
          plik<<"[LICZBA_OBIEKTOW]"<<endl;
          plik<<obiekty.n_obiektow()<<endl<<endl;
          plik<<"[WSPOLCZYNNIK_STRATY_ENERGII]"<<endl;
          plik<<wspolczynnik_straty_energii()<<endl<<endl;
          plik<<"[WSPOLCZYNNIK_MNOZENIA_WARTOSCI_POLA_EKWIPOTENCJALNEGO_(WSPOLCZYNNIK_BARWY)]"<<endl;
          plik<<pole_.wspolczynnik_mnozenia()<<endl;
          
          plik.close();
          }
     
}

void symulacja::zapisz_krok_do_pliku()
{

     string sesja_krok;
     ostringstream strumien;
     
            strumien<<sesja_;
            strumien<<'.'<<setw(10)<<setfill('0')<<l_krokow_glownych_;
            strumien<<".DAT";
            sesja_krok=strumien.str();

     ofstream plik;
              plik.open( sesja_krok.c_str() );
     
     if( !plik ){;}
         else{
              plik<<"[NAZWA_SESJI]"<<endl;
              plik<<sesja_<<endl;
              plik<<endl;
              plik<<"[__________DANE_SI___________________]"<<endl;
              plik<<endl;
              plik<<"[CZAS]"<<endl;
              plik<<l_krokow_glownych_ * dt_<<endl;
              plik<<endl;
              plik<<"[KROK]"<<endl;
              plik<<l_krokow_glownych_<<endl;

              plik<<endl;
              
              plik<<"[LP----]__[-----------------------WSPOLZEDNE-XYZ-----------------------]__[-------------------------PREDKOSC-XYZ-----------------------]__[-----MASA-----------]__[-----PROMIEN--------]__[KOLOR_R_G_B-----------------]"<<endl;
              
              for(int i=0; i<obiekty.n_obiektow(); i++)              
                      {
                           plik<<setw(5)<<setfill('0')<<i;
                           plik<<setw(2)<<setfill(' ');
                           
                           plik<<" "<<setw(20)<<setfill(' ')<<setprecision(8)<<scientific<<obiekty.obiekt(i).w().x();
                           plik<<setw(20)<<setfill(' ')<<setprecision(8)<<scientific<<obiekty.obiekt(i).w().y();
                           plik<<setw(20)<<setfill(' ')<<setprecision(8)<<scientific<<obiekty.obiekt(i).w().z(); 
                                                     
                           plik<<"   "<<setw(20)<<setfill(' ')<<setprecision(8)<<scientific<<obiekty.obiekt(i).v().x();
                           plik<<setw(20)<<setfill(' ')<<setprecision(8)<<scientific<<obiekty.obiekt(i).v().y();
                           plik<<setw(20)<<setfill(' ')<<setprecision(8)<<scientific<<obiekty.obiekt(i).v().z(); 

                           plik<<"    "<<setw(20)<<setfill(' ')<<setprecision(8)<<scientific<<obiekty.obiekt(i).masa();
                           plik<<"    "<<setw(20)<<setfill(' ')<<setprecision(8)<<scientific<<obiekty.obiekt(i).r();

                           plik<<"       "<<setw(8)<<setfill(' ')<<setprecision(0)<<scientific<<obiekty.obiekt(i).kolor().r();
                           plik<<setw(8)<<setfill(' ')<<setprecision(0)<<scientific<<obiekty.obiekt(i).kolor().g();
                           plik<<setw(8)<<setfill(' ')<<setprecision(0)<<scientific<<obiekty.obiekt(i).kolor().b(); 
                           
                           plik<<endl;          
              
                       }     
               
              plik.close();
             }
     
}

void symulacja::klawisze(unsigned char key, int x, int y)  // obsluga klawiaturki
{
    switch (key) 
    {
        case 27 : 
        
             
            exit(0);
            break;

        case '+':
             {
            sym.sfera_.slices()++;
            sym.sfera_.stacks()++;
            break;
            }
        case '-':
            if (sym.sfera_.slices()>3 && sym.sfera_.stacks()>3)
            {
                sym.sfera_.slices()--;
                sym.sfera_.stacks()--;
                break;
            }
            
        case '.':
             {
               sym.zapisz_krok_do_pliku();
               break;
             }
            
        case 'f':
             {
                
                sym.kamera_.translated_z()+=0.02;
              break;  
                }
        case 'r':
            
            {
                sym.kamera_.translated_z()-=0.02;
                break;
            }

            
    
        case 'a'://gora
            
            {
            sym.kamera_.translated_y()+=0.01;
                
                break;
            }

        case 'd'://dol
            
            {
            sym.kamera_.translated_y()-=0.01;
                 
                break;
            }
        case 'w'://lewo
            
            {
            sym.kamera_.translated_x()+=0.01;
                 
                break;
            }
        case 's'://prawo
            
            {
            sym.kamera_.translated_x()-=0.01;
                 
                break;
            } 
            
         case 'q':
            
            {
            sym.kamera_.translated_x()=0.0;
            sym.kamera_.translated_y()=0.0;
            sym.kamera_.translated_z()=0.0; 
            sym.kamera_.rotate_x()=0.0; 
            sym.kamera_.rotate_y()=0.0;                 
                break;
            } 
            
         case 'p': //wlacza rysowanie pola ekwipotencjalnego   
            {
             if( !sym.pole_.czy_rysuj_pole_ekwipotencjalne() ){ sym.pole_.czy_rysuj_pole_ekwipotencjalne()=true;   }
                                                else { sym.pole_.czy_rysuj_pole_ekwipotencjalne()=false; }   
             break;      
            }
            
            
         case 'o': //rysowanie pola ekwipotencjalnego   [o] - dz<0; [l] - dz>0;
            {
             if( sym.pole_.czy_rysuj_pole_ekwipotencjalne() )sym.pole_.pole_ekwipotencjalne_srodek().z()+=0.005;
                 
             break;      
            }
            
         case 'l': //rysowanie pola ekwipotencjalnego   [o] - dz<0; [l] - dz>0;
            {
             if( sym.pole_.czy_rysuj_pole_ekwipotencjalne() )sym.pole_.pole_ekwipotencjalne_srodek().z()-=0.005;
                 
             break;      
            }   

         case 'u': //rysowanie pola ekwipotencjalnego   [u] - dx<0; [j] - dx>0;
            {
             if( sym.pole_.czy_rysuj_pole_ekwipotencjalne() )sym.pole_.pole_ekwipotencjalne_srodek().x()+=0.02;
                 
             break;      
            }
            
         case 'j': //rysowanie pola ekwipotencjalnego   [u] - dx<0; [j] - dx>0;
            {
             if( sym.pole_.czy_rysuj_pole_ekwipotencjalne() )sym.pole_.pole_ekwipotencjalne_srodek().x()-=0.02;
                 
             break;      
            }   
         case 'i': //rysowanie pola ekwipotencjalnego   [i] - dz<0; [k] - dy>0;
            {
             if( sym.pole_.czy_rysuj_pole_ekwipotencjalne() )sym.pole_.pole_ekwipotencjalne_srodek().y()+=0.02;
                 
             break;      
            }
            
         case 'k': //rysowanie pola ekwipotencjalnego   [i] - dy<0; [k] - dy>0;
            {
             if( sym.pole_.czy_rysuj_pole_ekwipotencjalne() )sym.pole_.pole_ekwipotencjalne_srodek().y()-=0.02;
                 
             break;      
            }             
            
         case 't': //rysowanie pola ekwipotencjalnego  
            {
             if( sym.pole_.czy_rysuj_pole_ekwipotencjalne() )sym.pole_.pole_nx()+=1;
                 
             break;      
            }
            
         case 'g': //rysowanie pola ekwipotencjalnego   
            {
             if( sym.pole_.czy_rysuj_pole_ekwipotencjalne() )sym.pole_.pole_nx()-=1;
                 
             break;      
            }               

         case 'y': //rysowanie pola ekwipotencjalnego  
            {
             if( sym.pole_.czy_rysuj_pole_ekwipotencjalne() )sym.pole_.pole_ny()+=1;
                 
             break;      
            }
            
         case 'h': //rysowanie pola ekwipotencjalnego   
            {
             if( sym.pole_.czy_rysuj_pole_ekwipotencjalne() )sym.pole_.pole_ny()-=1;
                 
             break;      
            }
            
         case 'v': //rysowanie pola ekwipotencjalnego  
            {
             if( sym.pole_.czy_rysuj_pole_ekwipotencjalne() )sym.pole_.pole_dx()-=0.01;
                 
             break;      
            }
            
         case 'b': //rysowanie pola ekwipotencjalnego   
            {
             if( sym.pole_.czy_rysuj_pole_ekwipotencjalne() )sym.pole_.pole_dx()+=0.01;
                 
             break;      
            }
            
         case 'n': //rysowanie pola ekwipotencjalnego  
            {
             if( sym.pole_.czy_rysuj_pole_ekwipotencjalne() )sym.pole_.pole_dy()-=0.01;
                 
             break;      
            }
            
         case 'm': //rysowanie pola ekwipotencjalnego   
            {
             if( sym.pole_.czy_rysuj_pole_ekwipotencjalne() )sym.pole_.pole_dy()+=0.01;
                 
             break;      
            }                          
            
            
                
         case 'c': //rysowanie siatki na polu o z=0
            {
             if ( !sym.siatka_.czy_rysuj_siatke() ) { sym.siatka_.czy_rysuj_siatke()=true;    }
                                  else { sym.siatka_.czy_rysuj_siatke()=false; }
                 
             break;      
            } 
         
         case 'x': //rysowanie wektorow predkosci
            {
             if ( !sym.wektory_.czy_rysuj_wektory_predkosci() ) { sym.wektory_.czy_rysuj_wektory_predkosci()=true;    }
                                  else { sym.wektory_.czy_rysuj_wektory_predkosci()=false; }
                 
             break;      
            }                   

         case 'z': //pauza symulacji
            {
             if ( !sym.pauza() ) { sym.pauza()=true;    }
                                  else { sym.pauza()=false; }
                 
             break;      
            }
         
         case '1':
            {
            if ( !sym.klawisze_help_ ) { sym.klawisze_help_=true;    }
                   else { sym.klawisze_help_=false; }
            break;      
            } 

         case '2':
            {
            if ( !sym.dodatkowe_informacje_ ) { sym.dodatkowe_informacje_=true;    }
                   else { sym.dodatkowe_informacje_=false; }
            break;      
            }                                  
                        
    }

    glutPostRedisplay();
}


void symulacja::rysuj()
{
  glLoadIdentity();
    double xx=0, yy=0, zz=0, xxc=0, yyc=0, zzc=3, vv=0;    
    const double t = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
    const double a = t*90.0;
   
    glutSetWindow(window);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            // obroty kamery 

    xxc=0.11*sin( sym.kamera_.rotate_x() )              + sym.kamera_.translated_x();
    yyc=0.11*sin( sym.kamera_.rotate_y() )               + sym.kamera_.translated_y();
    zzc=0.11*( cos( sym.kamera_.rotate_y() )+ cos( sym.kamera_.rotate_x() ) )  + sym.kamera_.translated_z()  +3;
  
    xx=0.1*sin( sym.kamera_.rotate_x() )   + sym.kamera_.translated_x(); 
    yy=0.1*sin( sym.kamera_.rotate_y() )   + sym.kamera_.translated_y() ;
    zz=0.1*( cos( sym.kamera_.rotate_y() ) + cos( sym.kamera_.rotate_x() ) )  + sym.kamera_.translated_z()  +3 ;
 
    gluLookAt( xxc , yyc, zzc,
               xx  ,  yy , zz,
               1.0, 0.0, 0.0 );
 
     if( sym.siatka_.czy_rysuj_siatke() )
           { sym.siatka_.rysuj_siatke(); }     
 
     if( sym.pole_.czy_rysuj_pole_ekwipotencjalne() )
           { 
           sym.pole_.rysuj_pole_ekwipotencjalne( sym.obiekty ); 
           }    
     

  if(!sym.pauza_)
  {    
    for(int i=0; i<sym.l_krokow_podrzednych_; i++) 
    {
     sym.obliczaj();   
     sym.l_krokow_glownych_++;
    }            
  }

   // sym.zapisz_krok_do_pliku();

    for(int i=0; i<sym.obiekty.n_obiektow(); i++)//sym.obiekty_.size()
	          {
              glColor3d( sym.obiekty.obiekt(i).kolor().r(), sym.obiekty.obiekt(i).kolor().g(), sym.obiekty.obiekt(i).kolor().b() );          
   
              glPushMatrix();
                 glTranslated( sym.obiekty.obiekt(i).w().x()/skala, sym.obiekty.obiekt(i).w().y()/skala, sym.obiekty.obiekt(i).w().z()/skala);
                 //glRotated(a,0,0,1);
                 glutSolidSphere( sym.obiekty.obiekt(i).r()/skala, sym.sfera_.slices(), sym.sfera_.stacks() );
              glPopMatrix(); 
        
              if( sym.wektory_.czy_rysuj_wektory_predkosci() )
                {
                sym.wektory_.szerokosc()= 10e6;                                           
                sym.wektory_.rysuj_wektor( sym.obiekty.obiekt(i).w(), sym.obiekty.obiekt(i).v() );    
                }

              }
              
              
              if( sym.dodatkowe_informacje_ )
                {
                 sym.i_100_klatek_++;
                 double nt_t=glutGet(GLUT_ELAPSED_TIME);
                 sym.czas_100_klatek_+= ((nt_t - t*1000.0)*0.001);
                 
                 if(sym.i_100_klatek_>=100)
                 {                           
                                              
                   sym.klatek_na_sekunde_=(int)( 100/sym.czas_100_klatek_  );                            
                   sym.i_100_klatek_=0;
                   sym.czas_100_klatek_=0.0;
                   
                   }
                  sym.t_=t;  
                 sym.dodatkowe_informacje();
                }
                
                   if( sym.klawisze_help_ )
                   {
                    sym.klawisze_help();
                   } 
                   
                      
                     if( sym.pauza_ ) 
                     {
                      glPushMatrix();   
                       glTranslated(sym.kamera_.translated_x() , sym.kamera_.translated_y(), sym.kamera_.translated_z()+1.0 );
                       glColor3d( 1.0f, 0.0f, 0.0f ); 
                       sym.glutPisz(-0.29f, -0.2f, GLUT_BITMAP_9_BY_15, "PAUZA", 1.2);
                      glPopMatrix();
                     }   
                    
  for(int j=0; j<sym.trajektoria_.size(); j++){
   if( !sym.pauza_ ) 
      {
                    if( sym.trajektoria_[j].licznik_krokow()> sym.trajektoria_[j].co_ile_krokow() )
                    {
                        
                      sym.trajektoria_[j].dodaj_wezel( sym.obiekty.obiekt(j).wektor_polozenia() ); 
                      sym.trajektoria_[j].licznik_krokow()=0;   
                    }
       sym.trajektoria_[j].licznik_krokow()++;                  
       }
   sym.trajektoria_[j].rysuj();                
  
  }
      
   glutSwapBuffers();
}      
    

//=====================================================================================
void idle(void)
{
    glutPostRedisplay();
}
//=====================================================================================
/*!
 [MANUAL] 
 PRZYPADKI WYSTEPOWANIA POSZCZEGOLNYCH PARAMETROW WIERSZA POLECEN 

*/
void manual(void)
{
 cerr<<endl<<endl<<"M..A..N..U..A..L........................."<<endl<<endl;    
 cerr<<endl<<

 " PRZYPADKI WYSTEPOWANIA POSZCZEGOLNYCH PARAMETROW WIERSZA POLECEN : "<<endl<<
 " 1. -s [nazwa plik sesji z glownymi parametrami] -r  "<<endl<<
 " 2. -s [nazwa plik sesji z glownymi parametrami] -k [nazwa klatki symulacji] "<<endl;
 
 cerr<<endl<<"........................................................."<<endl<<endl;  
// throw 0;    
}

//====================================================================================


///Ustala spos�b projekcji grafiki
void resize(int width, int height)
{
    const float ar = (float) width / (float) height;
 
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    

       gluPerspective(18, ar, 0.01, 200.0); 

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;
}
//=======================================================================================

const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 0.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 5.0f };// sila promieni swietlnych
const GLfloat light_spot_cutoff[] = { 180.0f };   // kat stozka swiatla
const GLfloat light_position[] = { 2.0f, 2.0f, -1.0f, 0.0f };




                  
int main(int argc, char *argv[])
try{
       
 srand ( time(NULL) ); 



//==================================   OBSLUGA WIERSZA POLECEN ======================        
 string parametr_temp=" ";
          string parametr[]={"-s", "-k", "-r", "-h"};   //aby wygenerowac uklad wymagany jest plik sesyjny po  -s i parametry generowania (jest ich 8) po -r
          int l_pod_par[]={1, 1, 10};              //liczba wartosci definiowanych przez dany parametr
          string parametr_wart[]={" "," "};
          bool  czy_parametr[]={ false, false, false, false };
          short suma_wymaganych_podparametrow=0;
          short liczba_parametrow_glownych=0;
         


//======================== Obsluga wystapien parametrow i podparametrow : ==============

 if( argc<2 ){  manual();/* throw 0; */}
 else{
    if( argv[1]==parametr[3] ){ manual(); return 0; }; 
      
    parametr_temp=argv[1];

    for(int i=1; i<argc; i++)
    {
      parametr_temp=argv[i];
  
     for(int j=0; j<3; j++)
     {
      if( parametr_temp== parametr[j] ) 
        { 
          czy_parametr[j]=true;
          if( parametr_temp!="-r" && parametr_temp!="-h") parametr_wart[j]= argv[ i+1 ];      
          

           
          
        }
      }
 }
}

//=====  Sprawdzanie poprawnosci wzajemnych kombinacji wystapien parametrow w zaleznosci od funkcji====
 if( czy_parametr[0] && czy_parametr[1] )
 {
   
   symulacja::referencja().czytaj_z_plikow( parametr_wart[0], parametr_wart[1] );
   symulacja::referencja().zapisz_plik_sesji(); 
 }
 else
 {
   if( czy_parametr[2] && czy_parametr[0] )
   {
    symulacja::referencja().czytaj_plik_sesji( parametr_wart[0] ); 
    symulacja::referencja().generuj();
   // symulacja::referencja().zapisz_krok_do_pliku(); 
    
    symulacja::referencja().zapisz_plik_sesji();           
                

   }
    else
     {
      manual(); throw 0;
     };
 
 };

//==================================   OBSLUGA WIERSZA POLECEN ======================   
   
   
    
       
  
  
         
    glutInit(&argc, argv);
    glutInitWindowSize(900,700);
    glutInitWindowPosition(0,0);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

    window=glutCreateWindow("Implementacja podstawowych zjawisk przyrody - Rafa� Sta�czuk @GLUT (97834)");

    glutReshapeFunc(resize);
    
 

   glutKeyboardFunc( symulacja::klawisze );
   glutMouseFunc( symulacja::MouseButton );     
   glutMotionFunc( symulacja::MouseMotion );  
    
   glutIdleFunc(idle);

 
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glEnable(GL_DEPTH_TEST);

                            glEnable(GL_LIGHTING);
                            glEnable(GL_LIGHT0);
                            glEnable(GL_NORMALIZE);
                            glEnable(GL_COLOR_MATERIAL);

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glLightfv(GL_LIGHT0, GL_SPOT_CUTOFF, light_spot_cutoff);
    
    

    glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);

	glutDisplayFunc( symulacja::rysuj );
    glutMainLoop();
    
  return EXIT_SUCCESS;    

 }
catch (const int& kod_bledu){ cerr<<" Wystapil blad : "<<blad( kod_bledu ); }
catch (bad_alloc) { cerr << "Blad w przydziale pamieci\n"; }
catch (...) { cerr << "Wystapil nieznany blad\n"; }

 
 

