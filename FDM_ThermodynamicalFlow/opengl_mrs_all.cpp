/*
 *	About :
 *	The study about utilization of numerical methods 
 *	for resolving thermodynamical-flow
 *	by using Finite Derivative Method in 1D problems
 *	
 *	See what this code can do:
 *	ZMN_sprawko_mrs_1D_RS.pdf
 *	
 *	
 *	Copyright (C) 2009, 2017  Rafa� Sta�czuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//======================================================//
// Zaawansowane Metody Numeryczne                       //
//              2009 WIMiI                              //
//                   III. rok, gr VI                    //
//                   Informatyka, Informatyka Stosowana //
//                                Sta�czuk Rafa�        //
//======================================================//

#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <GL/glut.h>


using namespace std;


      
 bool pauza=true;
 bool ogrzewaj=false;

//=================================================      Wspolczynniki warunkow brzegowych =========== 
 double I_Dirichlet_T_L=500.0; //Lewy brzeg Temperatura
 double I_Dirichlet_T_P=900.0;//Prawy brzeg
 double II_Neumann_Q_L=200000.0; //Lewy brzeg strumien ciepla
 double II_Neumann_Q_P=100000.0;//Prawy brzeg strumien ciepla
 double III_Newton_alfa_L=100.0; //Lewy brzeg wsp. przejmowania cieplnego
 double III_Newton_alfa_P=200.0;//Prawy brzeg 
 bool I_Dirichlet_L=false;
 bool I_Dirichlet_P=false;
 bool II_Neumann_L=false;
 bool III_Newton_L=false;
 bool II_Neumann_P=false;
 bool III_Newton_P=false; 
//====================================================================================================
 double strumien_pierwszy_Q=6000000.0;
 double strumien_drugi_Q=4000000.0;
 
 bool tryb_schematu_niejawnego=false; 
 bool koniec_obliczen=false;
  
 int window;
 double ts=0.0;
 double lambda=50.0;//wsp. przewodzenia ciep�a [W/(m*K)]
 double ro=7500.0;  // gestosc stali [kg/m^3)
 double c=500.0;    //ciep�o w�a�ciwe stali [J/(kg*K)]
 double l=1.0;      //dlugosc nieskonczenie cienkiego preta [m]
 int lw=50;        //liczba wezlow
 double h;
 double T_otocz=300.0;  //Temperatura otoczenia w nieskonczenie-odleglym punkcie[K]
 double dt;             //Krok czasowy schematu niejawnego

 long int n_krok_aktualny=0;
 long int n_krokow_do_obliczenia=1000;
 int liczba_krokow_schematu_niejawnego=500;
 
 
 int szybkosc=1;
  ofstream plik;


class wektory
{
public:
  wektory(const int& ln){
                T0=new double[ln];
                T1=new double[ln];
                Q=new double[ln];
                for(int i=0; i<ln; i++){Q[i]=0.0; 
                                        T0[i]=0.0;
                                        T1[i]=0.0;
                                        }                
                
                };     
 double* T0;
 double* T1;
 double* Q;
 ~wektory()
 {
 delete [] T0;
 delete [] T1;
 delete [] Q;          
           
 }
}; 

wektory w(lw);


//===================================  schemat_niejawny  ==============================================
//===================================  schemat_niejawny  ==============================================
//===================================  schemat_niejawny  ==============================================
//===================================  schemat_niejawny  ==============================================
//===================================  schemat_niejawny  ==============================================

class CMacierz{
        
       int m_;
       int n_;     
       double **w;
  
       public :
              
           CMacierz():m_(0), n_(0){ w= new double*[0]; };
           
           CMacierz(const int& mm, const int& nn):m_(mm), n_(nn)
           {
            w=new double*[m_];
                    for(int i=0; i<m_; i++)
                    {
                     w[i]=new double[n_];
 	                 for(int i2=0; i2<n_;i2++)
 		                     {
		                      w[i][i2]=0;
                             }
 
                     }                         
                        
           } 
           

           
  
       
       
       const int& m()const{return m_;}
       const int& n()const{return n_;}
       
       int& m(){return m_;}
       int& n(){return n_;}
       
       const double& wartosc(const int& m, const int& n)const{return w[m][n];}
       double& wartosc(const int& m, const int& n){return w[m][n];}

          
     	     friend CMacierz operator +( const CMacierz& a, const CMacierz& b);
             friend CMacierz operator *( const CMacierz& a, const CMacierz& b);
             friend CMacierz operator *( const double& a, const CMacierz& b);   
             
             
             CMacierz& operator =( const CMacierz& );
             CMacierz Oblicz_uklad(void);
                    
             ~CMacierz(){
                        for(int i=0; i<m_; i++)delete [] w[i];
                        delete [] w; 
                      };                   
                   
       
       };
       
//=========================================================       
CMacierz& CMacierz::operator =( const CMacierz& a)
{
 if(&a==this)return *this;
             
            w=new double*[a.m()];
                    for(int i=0; i<a.m(); i++)
                    {
                     w[i]=new double[a.n()];
 	                 for(int i2=0; i2<a.n();i2++)
 		                     {
		                      w[i][i2]=a.w[i][i2];
                             }
 
                     } 
                     
   return *this;
}   


//========================================================================================
CMacierz operator +( const CMacierz& a, const CMacierz& b)
{
  
 CMacierz wynik( a.m(), a.n() );
 
         for(int i1=0; i1<a.m(); i1++)
                 {
               for(int i2=0; i2<a.n(); i2++)
                         {
                           wynik.w[i1][i2]=a.w[i1][i2]+b.w[i1][i2];
                         }      
                  }
                 
                 return wynik;
}
//========================================================================================
CMacierz operator *( const double& a, const CMacierz& b)
{

 CMacierz wynik( b.m(), b.n() );
          

         for(int i=0; i<wynik.m(); i++)
                 {
                for(int j=0; j<wynik.n(); j++)
                         {
                            wynik.w[i][j]=a*b.w[i][j];
                         }      
                 }
                 
                 return wynik; 
}


//========================================================================================
CMacierz operator *( const CMacierz& a, const CMacierz& b)
{
 
 CMacierz wynik( a.m(), b.n() );
          

         for(int i=0; i<wynik.m(); i++)
                 {
                for(int j=0; j<wynik.n(); j++)
                         {
                           for(int k=0; k<wynik.n(); k++)
                           {  
                           wynik.w[i][j]+=a.w[i][k] * b.w[k][j];
                           }
                               
                         }      
                         
                 }
                 
                 return wynik;
             
}


CMacierz CMacierz::Oblicz_uklad(void)
{
  
  double tmp=0.0;
  
  CMacierz x(m_, 1);
  


for( int s=0; s<(m_-1); s++)
     for( int i=s+1; i<m_; i++)
          for( int j=s+1; j<=m_; j++)
               w[i][j]=w[i][j]-w[i][s]*w[s][j]/w[s][s];

               
x.wartosc(m_-1, 0)=w[m_-1][m_]/w[m_-1][m_-1];

for( int i=m_-2; i>=0; i--)
{
     tmp=0.0;
     for( int j=i+1; j<m_; j++)
      tmp+=w[i][j]*x.wartosc(j, 0);
      
      x.wartosc(i, 0)=( w[i][m_] -tmp )/ w[i][i];
      
     
     }




  return x;

}

//========================================================================================
//============================ KONIEC KLASY MACIERZY =====================================
//========================================================================================
CMacierz roz_niejawny(lw, 1);
int krok_niejawny=0;

void schemat_niejawny()
{
 
    double dt_n = n_krokow_do_obliczenia*dt / liczba_krokow_schematu_niejawnego;
   
    
    
   CMacierz mat_rozsz(lw, lw+1);
   
   CMacierz lambda_n(lw, 1);
   CMacierz Q_n(lw, 1);
   CMacierz ro_n(lw,1);
   CMacierz c_n(lw, 1);
   CMacierz T0_n(lw, 1);
   
  
  //wsteczny Eulera  omega=1;
 int omega=1;
  
//================================================================  

   
 
 


  for(int i=0; i<lw; i++)
  {
   lambda_n.wartosc(i, 0)=lambda;
   ro_n.wartosc(i, 0)=ro;
   c_n.wartosc(i,0 )=c;        
  
  }
  

    for(int i=0; i<lw; i++)
  {
     if(krok_niejawny==0){T0_n.wartosc(i,0)=w.T0[i];}
      else {T0_n.wartosc(i, 0)=roz_niejawny.wartosc(i, 0);}
   
   Q_n.wartosc(i,0)=w.Q[i];  
  }
//====================================  Nakladanie warunkow brzegowych ================ 
  



  
if(I_Dirichlet_L)
 {
  mat_rozsz.wartosc(0, 0)=1.0;  
  mat_rozsz.wartosc(0, lw)= I_Dirichlet_T_L ;         
 }

if(I_Dirichlet_P)
 {
  mat_rozsz.wartosc(lw-1, lw-1)=1.0;   
  mat_rozsz.wartosc(lw-1, lw)=I_Dirichlet_T_P ;               
 }

  
if(II_Neumann_L)
 {
  mat_rozsz.wartosc(0, 0)=lambda/h;  
  
  mat_rozsz.wartosc(0, 1)=-lambda/h;              
 }

if(II_Neumann_P)
 {
  mat_rozsz.wartosc(lw-1, lw-1)=lambda/h;    
  
  mat_rozsz.wartosc(lw-1, lw-2)= -lambda/h;              
 }

if(III_Newton_L)
 {
   mat_rozsz.wartosc(0, 1)= -lambda/h; 
   
   mat_rozsz.wartosc(0, 0)=(lambda/h) + III_Newton_alfa_L;                 
 }
 
if(III_Newton_P)
 {
  mat_rozsz.wartosc(lw-1, lw-1)=(lambda/h) + III_Newton_alfa_P;    
  
  mat_rozsz.wartosc(lw-1, lw-2)= -lambda/h;    
  }






//===========================   Macierz "D" :=================================================


  if(II_Neumann_L)mat_rozsz.wartosc( 0, lw)= II_Neumann_Q_L;//  dirichlet ze stanu temp. jawnego schematu
  if(II_Neumann_P)mat_rozsz.wartosc( lw-1, lw)= II_Neumann_Q_P;//
  
 if(III_Newton_L)mat_rozsz.wartosc(0, lw)=III_Newton_alfa_L * T_otocz; 
 if(III_Newton_P)mat_rozsz.wartosc(lw-1, lw)=III_Newton_alfa_P * T_otocz; 
   
  for(int i=1; i<lw-1; i++)
  { 
                         
    mat_rozsz.wartosc( i, lw)= (1-omega) * lambda_n.wartosc(i, 0)*( (T0_n.wartosc(i-1,0) - 2*T0_n.wartosc(i,0) + T0_n.wartosc(i+1,0))/(h*h))+( ro_n.wartosc(i, 0)*c_n.wartosc(i, 0)/dt_n)*T0_n.wartosc(i,0)+Q_n.wartosc(i,0);
  }
//================================================================
  
 


   
  for(int i=1; i<lw-1; i++)
  {                                                    
   mat_rozsz.wartosc(i, i) = ro_n.wartosc(i, 0)*c_n.wartosc(i, 0)/dt_n + 2*omega*lambda_n.wartosc(i, 0)/(h*h);
   mat_rozsz.wartosc(i, i-1)=-omega*lambda_n.wartosc(i, 0)/(h*h);  
   mat_rozsz.wartosc(i, i+1)=-omega*lambda_n.wartosc(i, 0)/(h*h);
  
   
  }

  
 
 roz_niejawny=mat_rozsz.Oblicz_uklad();
 
 
 
 krok_niejawny++;
    
}
//==============|===================  schemat_niejawny  ==============================================
//=============/|\==================  schemat_niejawny  ==============================================
//============/=|=\=================  schemat_niejawny  ==============================================
//==============|===================  schemat_niejawny  ==============================================
//==================================  schemat_niejawny  ==============================================







void obliczaj_krok(void)
{



  

 for(int i=1; i<lw-1; i++)
  {
   w.T1[i]=( dt/(ro*c) )* ( lambda* (w.T0[i-1]-2*w.T0[i]+w.T0[i+1]) /(h*h) +w.Q[i] ) +w.T0[i];
  }  

//=========================================   DRUGI WARUNEK BRZEGOWY ============================
if( II_Neumann_L )
{   
 w.T1[0]=w.T0[1] + (h/lambda)* II_Neumann_Q_L; 
}

if( II_Neumann_P )
{
 w.T1[lw-1]=w.T0[lw-2] + (h/lambda)* II_Neumann_Q_P; 
}
//===============================================================================================

  
//=========================================   TRZECI WARUNEK BRZEGOWY =========================== 

//alfa1 1000.0 na lewym brzegu   III
//alfa2 100.0   na prawym brzegu  III
if( III_Newton_L )//Lewy brzeg
{ 
  w.T1[0]=( 1.0/( 1.0 + h*III_Newton_alfa_L/lambda ) )*( w.T0[0] + (h/lambda)*III_Newton_alfa_L*T_otocz );
}
if( III_Newton_P )//Prawy brzeg
{
  w.T1[lw-1]=( 1.0/( 1.0 + h*III_Newton_alfa_P/lambda ) )*( w.T0[lw-1] + (h/lambda)*III_Newton_alfa_P*T_otocz );
}
//===============================================================================================



 for(int i=0; i<lw; i++)
  {
  w.T0[i]=w.T1[i]; 
  }


  ts+=dt; 
  
  
  //if( czy_schemat_niejawny )
 // {
  //                     
 // }
 
 
}

void glutPisz( const float& x, const float& y , void * font, const string& txt, const float& scale)
{
 int i;
  int len=txt.size();
 float tmp_x=x; 
 for (i=0; i<len; i++)
 {
  glRasterPos2f(tmp_x, y);
  glutBitmapCharacter(font, txt[i]);
  tmp_x+=glutBitmapWidth(font, txt[i])*scale/400;
 }    
     
}

string int_to_string(const long& i)
{
  char buffer [50];
  string tmp;
  
 itoa (i,buffer,10);
  tmp=buffer;

  return tmp; 
 
}


void rysuj()
{
  string tmp;
  glLoadIdentity();
     
    //const double t = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
    
   
    glutSetWindow(window);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  
    
  if(!pauza)
  {  
    for(int i=0; i<szybkosc;i++)
    {

           if(tryb_schematu_niejawnego)
              {
               if(n_krok_aktualny+1>n_krokow_do_obliczenia)koniec_obliczen=true;  
              }            
            
           if(!koniec_obliczen)
           {
            obliczaj_krok();
           if(tryb_schematu_niejawnego)n_krok_aktualny++;
           }
            

    }
  }   
//====================     Parametry       ==============     
 glPushMatrix();
  glTranslated(-0.5, -0.7, 0.0);
  glColor3d( 1.0f, 0.0f, 0.0f );    
   tmp=int_to_string( (int)ts );
   tmp+=" [sekund swiata symulacji]";
   glutPisz(-0.23f, 0.4f, GLUT_BITMAP_9_BY_15, tmp, 1.0);
   tmp=" ";


  glColor3d( 0.0f, 1.0f, 0.0f );  
   
   tmp="Liczba wezlow : "+int_to_string( lw );
   glutPisz(-0.4f, 0.25f, GLUT_BITMAP_9_BY_15, tmp, 1.0);  
    
   tmp="dt : "+int_to_string( (int)(dt*1000000.0) )+ " [mikro_s] ";
   glutPisz(-0.4f, 0.20f, GLUT_BITMAP_9_BY_15, tmp, 1.0);     

   tmp="h : "+int_to_string( (int)(h*1000000.0) )+ " [mikro_m] ";
   glutPisz(-0.4f, 0.15f, GLUT_BITMAP_9_BY_15, tmp, 1.0); 

   tmp="T_otocz : "+int_to_string( (int)(T_otocz) )+ " [st. K] ";
   glutPisz(-0.4f, 0.1f, GLUT_BITMAP_9_BY_15, tmp, 1.0);  
   

   
   tmp="Lambda : "+int_to_string( (int)lambda );
   glutPisz(-0.4f, 0.05f, GLUT_BITMAP_9_BY_15, tmp, 1.0);

   tmp="Alfa1 : "+int_to_string( (int)III_Newton_alfa_L )+" [W/(m^2*K)] ";
   glutPisz(-0.3f, 0.0f, GLUT_BITMAP_9_BY_15, tmp, 1.0);
   tmp="Alfa2 : "+int_to_string( (int)III_Newton_alfa_P )+" [W/(m^2*K)] ";
   glutPisz(-0.3f, -0.05f, GLUT_BITMAP_9_BY_15, tmp, 1.0);

   
   tmp="ro : "+int_to_string( (int)ro )+" [kg/m^3] ";
   glutPisz(-0.4f, -0.10f, GLUT_BITMAP_9_BY_15, tmp, 1.0);   

   tmp="c : "+int_to_string( (int)c )+"  ";
   glutPisz(-0.4f, -0.15f, GLUT_BITMAP_9_BY_15, tmp, 1.0);

   tmp="l : "+int_to_string( (int)(l*100.0) )+" [cm] ";
   glutPisz(-0.4f, -0.20f, GLUT_BITMAP_9_BY_15, tmp, 1.0);        

 
 
   glColor3d( 1.0f, 0.0f, 1.0f );
   tmp="Dirichlet(stan dla niejawnego) L/P-'1'/'q' : ";
       if( I_Dirichlet_L ) {tmp+='T';}else{tmp+="N";}
       if( I_Dirichlet_P ) {tmp+='T';}else{tmp+="N";} 
   glutPisz(0.2, 0.25f, GLUT_BITMAP_9_BY_15, tmp, 1.0);  
  



//==============================================================================
   glColor3d( 1.0f, 0.0f, 1.0f );
   tmp="BRZEG LEWY";
   glutPisz(0.3, 0.20f, GLUT_BITMAP_9_BY_15, tmp, 1.0); 
 if(II_Neumann_L)
 {
   glColor3d( 1.0f, 0.0f, 0.0f );
   tmp="Nalozono II. Warunek brzegowy - Neumann'a(2-aby wylaczyc)";
   glutPisz(0.4, 0.15f, GLUT_BITMAP_9_BY_15, tmp, 1.0);  
 }else{
   glColor3d( 1.0f, 1.0f, 1.0f );
   tmp="\"2\"- II. Warunek brzegowy - Neumann'a";
   glutPisz(0.4, 0.15f, GLUT_BITMAP_9_BY_15, tmp, 1.0);         
 }
 
 if(III_Newton_L)
 {
   glColor3d( 1.0f, 0.0f, 0.0f );
   tmp="Nalozono III. Warunek brzegowy - Newtona(3-aby wylaczyc)";
   glutPisz(0.4, 0.10f, GLUT_BITMAP_9_BY_15, tmp, 1.0);  
 }else{
   glColor3d( 1.0f, 1.0f, 1.0f );
   tmp="\"3\"- III. Warunek brzegowy - Newtona";
   glutPisz(0.4, 0.10f, GLUT_BITMAP_9_BY_15, tmp, 1.0);         
 } 
//================================ 
   glColor3d( 1.0f, 0.0f, 1.0f );
   tmp="BRZEG PRAWY";
   glutPisz(0.3, 0.05f, GLUT_BITMAP_9_BY_15, tmp, 1.0); 

 if(II_Neumann_P)
 {
   glColor3d( 1.0f, 0.0f, 0.0f );
   tmp="Nalozono II. Warunek brzegowy - Neumann'a(w-aby wylaczyc)";
   glutPisz(0.4, 0.0f, GLUT_BITMAP_9_BY_15, tmp, 1.0);  
 }else{
   glColor3d( 1.0f, 1.0f, 1.0f );
   tmp="\"w\"- II. Warunek brzegowy - Neumann'a";
   glutPisz(0.4, 0.0f, GLUT_BITMAP_9_BY_15, tmp, 1.0);         
 }
 
 if(III_Newton_P)
 {
   glColor3d( 1.0f, 0.0f, 0.0f );
   tmp="Nalozono III. Warunek brzegowy - Newtona(e-aby wylaczyc)";
   glutPisz(0.4, -0.05f, GLUT_BITMAP_9_BY_15, tmp, 1.0);  
 }else{
   glColor3d( 1.0f, 1.0f, 1.0f );
   tmp="\"e\"- III. Warunek brzegowy - Newtona";
   glutPisz(0.4, -0.05f, GLUT_BITMAP_9_BY_15, tmp, 1.0);         
 } 
//=================================================================================

 if(ogrzewaj)
 {
   glColor3d( 1.0f, 0.0f, 0.0f );
   tmp="Nalozono dwa strumienie (o- aby wylaczyc)";
   glutPisz(0.3, -0.15f, GLUT_BITMAP_9_BY_15, tmp, 1.0);  
 }else{
   glColor3d( 1.0f, 1.0f, 1.0f );
   tmp="\"o\"- Ogrzewaj - dodaj dwa strumienie";
   glutPisz(0.3, -0.15f, GLUT_BITMAP_9_BY_15, tmp, 1.0);         
 }

   glColor3d( 0.0f, 1.0f, 0.0f );
   tmp="a/z - przyspiesz/spowolnij przebieg symulacji";
   glutPisz(0.4, 0.35f, GLUT_BITMAP_9_BY_15, tmp, 1.0);  

   glColor3d( 0.0f, 1.0f, 0.0f );
   tmp="Aktualna szybkosc : "+int_to_string(szybkosc);
   glutPisz(0.4, 0.30f, GLUT_BITMAP_9_BY_15, tmp, 1.0); 

   glColor3d( 1.0f, 1.0f, 1.0f );
   if(tryb_schematu_niejawnego){ glColor3d( 1.0f, 0.0f, 0.0f );}
   tmp="n - Spauzuj, nadaj warunki aktualne jako poczatkowe";
   glutPisz(0.3, -0.20f, GLUT_BITMAP_9_BY_15, tmp, 1.0);  

  
   tmp="i wlacz tryb schematu niejawnego";
   glutPisz(0.4, -0.25f, GLUT_BITMAP_9_BY_15, tmp, 1.0);  
   
if(tryb_schematu_niejawnego)
{
   glColor3d( 1.0f, 1.0f, 1.0f );
      tmp="Krok akt. : "+int_to_string( n_krok_aktualny );
   glutPisz(1.1, 0.9f, GLUT_BITMAP_9_BY_15, tmp, 1.0);                            
                            
}   
       
 glPopMatrix(); 
//=====================================================

//=========   Siatka  i skala temperatury :   ===============
              glPushMatrix();
                   for(double TT=100.0; TT<1200.0 ; TT+=50.0)
                   { 
                     glColor4f( TT*TT/100000.0, 0.0, 1.0 , 1.0 );         
                                  tmp=int_to_string( (int)TT );
                                  glutPisz( 0.5, TT/1000.0 -0.2, GLUT_BITMAP_9_BY_15, tmp, 1.0);
                                  glutPisz( -1.0, TT/1000.0 -0.2,  GLUT_BITMAP_9_BY_15, tmp, 1.0);  
                                                   
                     glBegin(GL_LINES);
                                      glVertex3f( -0.9,  TT/1000.0 -0.2   , 0.0 );
                                      glVertex3f( -0.8 ,  TT/1000.0  -0.2 , 0.0 );
                     glEnd(); 
                     
                     glBegin(GL_LINES);
                                      glVertex3f( 0.35,  TT/1000.0 -0.2   , 0.0 );
                                      glVertex3f( 0.45 ,  TT/1000.0  -0.2 , 0.0 );
                     glEnd();                      

                     
                   }
              glPopMatrix();
              
//==========        Pret :   =======================
               glPushMatrix();
                   for(int i=0; i<lw-1 ; i++)
                   {  
                      glBegin(GL_QUADS);
                        glColor4f( w.T0[i]*w.T0[i]/100000.0, 0.0, 1.0 , 1.0 );      glVertex3f( i/((double)lw)-0.7 , -0.2  ,0.0  );
                        glColor4f( w.T0[i+1]*w.T0[i+1]/100000.0, 0.0, 1.0 , 1.0 );  glVertex3f( i/((double)lw)+(1/((double)lw))-0.7 , -0.2 , 0.0  );
                        glColor4f( w.T0[i+1]*w.T0[i+1]/100000.0, 0.0, 1.0 , 1.0 );  glVertex3f( i/((double)lw)+(1/((double)lw))-0.7 , -0.1  , 0.0 );
                        glColor4f( w.T0[i]*w.T0[i]/100000.0, 0.0, 1.0 , 1.0 );      glVertex3f( i/((double)lw)-0.7 , -0.1  , 0.0  );
                      glEnd(); 
                   }
               glPopMatrix();
               
//=============================================WYKRES :    =====================
//===========      Jawnie====================                  
               glPushMatrix(); 
                   for(int i=0; i<lw-1 ; i++)
                   {  
                     glBegin(GL_LINES);
                                      glColor4f( w.T0[i]*w.T0[i]/100000.0, 0.0, 1.0 , 1.0 );
                                      glVertex3f( i/((double)lw)-0.7, w.T0[i]/1000.0 -0.2 , 0.0 );
                                      glVertex3f( i/((double)lw)+(1/((double)lw))-0.7, w.T0[i+1]/1000.0 -0.2 , 0.0 );
                     glEnd(); 
                   }      
               glPopMatrix();   
//===========      NieJawnie==================== 
   if(tryb_schematu_niejawnego)
   {
               glPushMatrix(); 
                   for(int i=0; i<lw-1 ; i++)
                   {  
                     glBegin(GL_LINES);
                                      glColor4f( 1.0 , 1.0, 1.0 , 1.0 );
                                      glVertex3f( i/((double)lw)-0.7, roz_niejawny.wartosc(i, 0)/1000.0 -0.2 , 0.0 );
                                      glVertex3f( i/((double)lw)+(1/((double)lw))-0.7, roz_niejawny.wartosc(i+1, 0)/1000.0 -0.2, 0.0 );
                     glEnd(); 
                   }      
               glPopMatrix();   

   }

      
//==============================================================================     
      
     
   glutSwapBuffers();
}      
    

void idle(void)
{
    glutPostRedisplay();
}

void resize(int width, int height)
{
    const float ar = (float) width / (float) height;
 
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    

     // gluPerspective(18, ar, 0.01, 200.0); 

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;
}



void klawisze(unsigned char key, int x, int y)  // obsluga klawiaturki
{
if(!tryb_schematu_niejawnego)     
{
    switch (key) 
    {
        case 27 : 
        
             
            exit(0);
            break;

         
        
        case '1':
             {
             III_Newton_L=false; 
             II_Neumann_L=false;
               
             if ( !I_Dirichlet_L ) { I_Dirichlet_L=true;    }
                                  else { I_Dirichlet_L=false; }
       
             
            break;
            } 

        case 'q':
             {
             III_Newton_P=false; 
             II_Neumann_P=false;
               
             if ( !I_Dirichlet_P ) { I_Dirichlet_P=true;    }
                                  else { I_Dirichlet_P=false; }
       
             
            break;
            }                     
         
           
        case '2':
             {
             III_Newton_L=false;
             I_Dirichlet_L=false;   
             if ( !II_Neumann_L ) { II_Neumann_L=true;    }
                                  else { II_Neumann_L=false; }
       
             
            break;
            } 
              
        case '3':
             {
              w.Q[0]=0.0;
              I_Dirichlet_L=false; 
              II_Neumann_L=false;
             if ( !III_Newton_L ) { III_Newton_L=true;    }
                                  else { III_Newton_L=false; }
                                  
           
            break;
            }              

        case 'w':
             {
             III_Newton_P=false; 
             I_Dirichlet_P=false;  
             if ( !II_Neumann_P ) { II_Neumann_P=true;    }
                                  else { II_Neumann_P=false; }
          
             
            break;
            } 
              
        case 'e':
             {
              w.Q[lw-1]=0.0;
              I_Dirichlet_P=false;
              II_Neumann_P=false;
             if ( !III_Newton_P ) { III_Newton_P=true;    }
                                  else { III_Newton_P=false; }
           
            break;
            }  
                 
             
        case 'p':
             {
                 
             if ( !pauza ) { pauza=true;    }
                                  else { pauza=false; }
            
            break;
            }            

        case 'o':
             {
             if ( !ogrzewaj ) { 
                               ogrzewaj=true;    
                                           w.Q[rand()%(lw-5)+1]=strumien_pierwszy_Q;   
                                           w.Q[rand()%(lw-5)+1]=strumien_drugi_Q; 
                              }
                                  else { 
                                        ogrzewaj=false; 
                                        for(int i=1; i<lw-1; i++)w.Q[i]=0.0;
                                       }
            
            break;
            } 



       case 'a'://przyspiesz
             {
             szybkosc+=10;
                   break;
            } 
       case 'z'://spowolnij
             {
             szybkosc-=10;
             if(szybkosc<0)szybkosc=1;
                   break;
            } 
 
       case 'n'://spauzuj, wyzeruj czas, nadaj warunki poczatkowe( czyli aktualne z jawnego ) licz i rysuj wynik dla schematu niejawnego
             {
              pauza=true;
              tryb_schematu_niejawnego=true;
              ts=0.0; 
          
              //=======obliczaj i rysuj schemat niejawny po n_krokow_do_obliczenia - krokach
              for(int i=0; i<liczba_krokow_schematu_niejawnego; i++)schemat_niejawny();
             // nastepnie narysowany schemat niejawny dla przyszlego stanu rozkl. Temoeratury
             // jest rysowany na bialo przez n_krokow_do_obliczenia krokow jawnego i do pauzy
             
              
                   break;
            }                                           
                        
    }
}else
   {
                        
      switch (key) 
        {
        case 27 : 
        
             
            exit(0);
            break;

        case 'a'://przyspiesz
             {
             szybkosc+=10;
                   break;
            } 
        case 'z'://spowolnij
             {
             szybkosc-=10;
             if(szybkosc<0)szybkosc=1;
                   break;
            } 
        case 'p':
             {
                 
             if ( !pauza ) { pauza=true;    }
                                  else { pauza=false; }
            
            break;
            }   
            
        }                    
                        
   }
    glutPostRedisplay();
}






const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 0.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 5.0f };// sila promieni swietlnych
const GLfloat light_spot_cutoff[] = { 180.0f };   // kat stozka swiatla
const GLfloat light_position[] = { 2.0f, 2.0f, -1.0f, 0.0f };


int main(int argc, char *argv[])
{
       

//==================================   OBSLUGA WIERSZA POLECEN ======================   
   
 srand(time(0));  
  h=l/(( (double)lw) -1.0);
  dt=h*h*ro*c/(2*lambda);   


 
     
 for(int i=0; i<lw; i++)w.T0[i]=T_otocz; 
 for(int i=0; i<lw; i++)w.T1[i]=T_otocz;
//============================== I. WARUNEK BRZEGOWY - DIRICHLETA (na prawy brzeg)======================
 w.T0[0]=I_Dirichlet_T_L;   
 w.T0[lw-1]=I_Dirichlet_T_P;
 w.T1[0]=I_Dirichlet_T_L;   
 w.T1[lw-1]=I_Dirichlet_T_P;
 
 I_Dirichlet_L=true;
 I_Dirichlet_P=true;
//======================================================================================


//=============================== Strumienie poczatkowe ciepla =================================
 /*if( ogrzewaj){
                                           w.Q[rand()%400+30]=strumien_pierwszy_Q;   
                                           w.Q[rand()%400+30]=strumien_drugi_Q;   
 }*/
//==============================================================================================
         
    glutInit(&argc, argv);
    glutInitWindowSize(900,700);
    glutInitWindowPosition(0,0);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

    window=glutCreateWindow("Zaawansowane Metody Numeryczne - Rafa� Sta�czuk @GLUT (97834)");

    glutReshapeFunc(resize);
    glutKeyboardFunc( klawisze );
 

 
    
   glutIdleFunc(idle);

 
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glEnable(GL_DEPTH_TEST);

                            glEnable(GL_LIGHTING);
                            glEnable(GL_LIGHT0);
                            glEnable(GL_NORMALIZE);
                            glEnable(GL_COLOR_MATERIAL);

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glLightfv(GL_LIGHT0, GL_SPOT_CUTOFF, light_spot_cutoff);
    
    

    glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);


	glutDisplayFunc( rysuj );
    glutMainLoop();
    
  return EXIT_SUCCESS;    

 }

  
  

