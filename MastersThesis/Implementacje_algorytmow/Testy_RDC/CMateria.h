/*
 *	About : 
 *	2011. The code I did for research reasons to supplement my 
 *	 	masters thesis on Częstochowa University of Technology
 *
 *	
 *	The study about:
 *		"Collision detection of irregular objects in two-dimensional Cartesian
 *		space"
 *      
 *	See:
 *	../../mgr_2.pdf
 *	
 *	Created on 26.03.2011, 
 *	
 *	Copyright (C) 2011, 2017  Rafał Stańczuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *	
 *	
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CMATERIA_H
#define CMATERIA_H

//========================================================================================
//========================================================================================


#include "CWektor2D.h"
#include "CStale.h"

///Klasa zawieraj�ca parametry danego obiektu materialnego
class CMateria{
    private:
     ///Wsp��rz�dne kartezja�skie
       CWektor2D    w_;
     ///Wektor pr�dko�ci
       CWektor2D    v_;

       double masa_;


       /*!
        * Promien okregu opisanego na wielokacie
        * (Odleglosc maksymalnie oddalonego wezla otoczki wielokata od jego srodka)
        * */
       double      promien_;
        
      public:
       
       CMateria(): masa_( 0.0 ), promien_( 0.0 ){;}
         
       CMateria( const  double& masa__, const CWektor2D& w__, const CWektor2D& v__,
                        const double& promien__ ):
                        masa_( masa__ ),  w_( w__ ), v_( v__ ), promien_( promien__ ){;};
       



       double& masa(){ return masa_; }
       const  double& masa() const { return masa_; }

       
       ///Metoda umo�liwiaj�ca przekazanie wektora po�o�enia do obiektu klasy CWector
       CWektor2D wektor_polozenia(){ return w_; }
       
       CWektor2D& w(){ return w_; }
       const CWektor2D& w() const { return w_; }
       
        CMateria& operator=(const CMateria& m){

           if( &m!=this )
           {
                masa()= m.masa();
                r()=m.r();
                v()=m.v();
                w()=m.w();
           }
        return *this;  
       }

       
       CWektor2D& v(){ return v_; }
       const CWektor2D& v() const { return v_; }
       
       double& r(){ return promien_; }
       const double& r() const { return promien_; }
       
      };
      
      
//========================================================================================
//========================================================================================  


#endif
