/*
 *	About : 
 *	2011. The code I did for research reasons to supplement my 
 *	 	masters thesis on Częstochowa University of Technology
 *
 *	
 *	The study about:
 *		"Collision detection of irregular objects in two-dimensional Cartesian
 *		space"
 *      
 *	See:
 *	../../mgr_2.pdf
 *	
 *	Created on 18.01.2011, 
 *	
 *	Copyright (C) 2011, 2017  Rafał Stańczuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *	
 *	
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CRUCH_H
#define CRUCH_H


#include "CWektor2D.h"

#include "CZbior_obiektow.h"
#include "CZderzenia_KUL.h"
#include "CRdc.h"
#include "CStale.h"

#include <iomanip>
#include "CPomiar.h"
#include "time_interval.h"

class CRuch:  public CRdc{
	
    CZbior_obiektow *obiekty;

	 CWektor2D g;
public:
	
	CRuch():odleglosc_tmp(0.0){}
	void inicjuj( CZbior_obiektow* _obiekty)
	{
		g=CWektor2D(0.0, -10.0);
		obiekty=_obiekty;
                zapis=_zapis;


	}
	
	
	void obliczaj();


	
	
	~CRuch(){}
};







void CRuch::obliczaj(void)
{
  std::cout<<std::endl<<"POCZATEK OBLICZEN..."<<std::endl;

  time_interval czast;
  vector<CPomiar> pomiary;
        int deep_tmp=10;
        double czas_t_iter, czas_t_zwykle;

int l_ob=obiekty->n_obiektow();

 for(; l_ob>1; l_ob-=100)//do testu 1/3
 {

        obiekty->obiekty().clear();//do testu 1/3
        obiekty->generuj(l_ob, 0.0, 1.0,  0.5, 1.1, 0.0, 0.0);//do testu 1/3
         
        Inicjuj_RDC(obiekty, deep_tmp);

              //Petla dynamicznej symulacji - wykrywajaca przypadki testowe
          for(int i_frame=0; i_frame<zapis->GetMaxN(); i_frame++)
          {
              cout<<endl<<i_frame<<" of "<<zapis->GetMaxN()<<" steps"<<endl;
//            for(int i_sstep=0; i_sstep<zapis->GetNumSmallSteps(); i_sstep++)
//            {

cout<<endl<<"                               l_obiektow akt. :"<<l_ob<<endl;
//////////////////////////////////////////////////////////////////////////////
//                czast.start();
//                      RDC_START_zwykle();
//                czast.stop();
//                czas_t_zwykle=czast.value();
               // cout<<endl<<"   L kolizji: "<<l_kolizji;

                czast.start();
                    RDC_START_iteratory();
                czast.stop();
                czas_t_iter=czast.value();
             //   cout<<endl<<"   L kolizji: "<<l_kolizji;
//========================================================================== DO testu nr 1/3=======
                if(l_kolizji==1){
                    cout<<endl<<"l_obiektow akt. :"<<obiekty->n_obiektow()<<endl;
                   
                    cout<<endl<<"     Wykonuje proste przegladanie - wykryto tylko 1. kolizje....";
                     Inicjuj_RDC(obiekty, 100000);//Wykonaj tylko prostym przegladaniem
                         czast.start();
                            RDC_START_iteratory();
                         czast.stop();
                     czas_t_zwykle=czast.value();//zapisz wartosc prostego przegladania

                     cout<<endl<<"      Koniec prostego przegladania...";
                    
                    
                    pomiary.push_back( CPomiar(obiekty->n_obiektow(), l_kolizji, deep_tmp, czas_t_zwykle, czas_t_iter ));
                     cout<<endl<<"      Pomiar zapisany...";
                      break;//po zakończeniu testu wyjdź z pętli symulacji dynamicznej

                }

//=======================================
                                
                      
              
////////////////////////////////////////////////////////////////////////////////
                  
                      for(int i1=0; i1<obiekty->n_obiektow(); i1++){


                          obiekty->obiekt(i1).w()=obiekty->obiekt(i1).w()+ obiekty->obiekt(i1).v()*dt;//

                          obiekty->obiekt(i1).v()=obiekty->obiekt(i1).v() + g*dt;


                       obliczaj_kolizje_z_powierzchnia( obiekty->obiekt( i1 ) );         //kolizja z powierzchnia dolna

                }

           // }

            }



 }

//sort( pomiary.begin(), pomiary.end(), por_N);

ofstream plik;

   plik.open("Wynik_test1z3.txt");
   plik<<endl<<endl<<"Liczba kolizji  |  Liczebność zbioru   |  deep      |   t[s] Proste przegladanie     |    t[s] for z iteratorami   "<<endl;

  // plik<<endl<<endl<<"Liczba kolizji  |  Liczebność zbioru   |  deep      |   t[s] Zwykla konstrukcja for     |    t[s] for z iteratorami   "<<endl;
   for(int i=0; i<pomiary.size(); i++)
   {
    plik<<pomiary[i].N<<"   "<<pomiary[i].liczba_obiektow<<"       "<<pomiary[i].deep<<"       "<<setprecision(10)<<(pomiary[i].t_zwykle)*10.0<<"       "<<setprecision(10)<<(pomiary[i].t_iteratory)*10.0<<endl;
   }



   plik.close();



  std::cout<<std::endl<<"KONIEC OBLICZEN..."<<std::endl;
}



#endif
