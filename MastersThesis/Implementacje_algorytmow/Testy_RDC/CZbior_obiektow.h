/*
 *	About : 
 *	2011. The code I did for research reasons to supplement my 
 *	 	masters thesis on Częstochowa University of Technology
 *
 *	
 *	The study about:
 *		"Collision detection of irregular objects in two-dimensional Cartesian
 *		space"
 *      
 *	See:
 *	../../mgr_2.pdf
 *	
 *	Created on 18.01.2011, 
 *	
 *	Copyright (C) 2011, 2017  Rafał Stańczuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *	
 *	
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CZBIOR_OBIEKTOW_H
#define CZBIOR_OBIEKTOW_H

#include "CMateria.h"
#include <vector>




///Klasa przechowuj¹ca tablicê obiektów
class CZbior_obiektow{
      std::vector<CMateria> obiekty_;
      std::vector<CMateria>::const_iterator obiekty_it;

      public:
        CZbior_obiektow(){;};
        CZbior_obiektow( const CZbior_obiektow& obs ){
          
            int tmp=obs.obiekty().size();

            for(unsigned int i=0; i< tmp ; i++){
                obiekty_.push_back( obs.obiekt(i) );
            }
            
        }
      
        void generuj(const int& , double, double, double, double, double, double);
        unsigned int n_obiektow(){ return obiekty_.size(); };

        const std::vector<CMateria>& obiekty(  )const { return obiekty_;};
        std::vector<CMateria>& obiekty(  ) { return obiekty_;};

        const CMateria& obiekt( const int& i )const { return obiekty_[i];};
        CMateria& obiekt( const int& i ) { return obiekty_[i];};
     
        ~CZbior_obiektow(){  };
        
};   


void CZbior_obiektow::generuj(const int& n_, double x0, double y0,  double rmin, double rmax, double vmin, double vmax)
{
    double k=0, l=0;
 for(int i=0; i< n_; i++)
 {


      float r_rand=rand()% (  (int)( (rmax-rmin)*10000.0  )  )/10000.0+ rmin;
          obiekty_.push_back( CMateria(
                         rand()% ( 10 ) + 10,
                     CKolor(k/(r_rand*20), l/(r_rand*20), 1.0, 0.8),
                        CWektor2D( x0+k*4.0, l*4.0+y0 + rand()%20000/20000.0 -1.0 ),
          CWektor2D( 0.0 , -20.0 ),
                       r_rand   )
                    );


  k+=r_rand*2;
  if(k>60){
      k=0;
      l+=r_rand*2;

  }

 }
  
std::cout<<std::endl<<"Liczba zainicjowanych obiektow :"<<obiekty_.size()<<"....."<<std::endl;
std::cout<<std::endl<<"Wygenerowano losowe polozenie i mase obiektow..."<<std::endl;
}

#endif
