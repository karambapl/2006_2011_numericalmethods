/*
 *	About : 
 *	2011. The code I did for research reasons to supplement my 
 *	 	masters thesis on Częstochowa University of Technology
 *
 *	
 *	The study about:
 *		"Collision detection of irregular objects in two-dimensional Cartesian
 *		space"
 *      
 *	See:
 *	../../mgr_2.pdf
 *	
 *	Created on 26.03.2011, 
 *	
 *	Copyright (C) 2011, 2017  Rafał Stańczuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *	
 *	
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CTYPES_H
#define	CTYPES_H

#include <deque>

#include "CGranica.h"
#include "CJednostka.h"
#include "CPara.h"

typedef std::deque<CGranica> Granice;
typedef std::deque<CGranica>::const_iterator it_Granice;  //const_iterator

typedef std::deque<CJednostka> Grupa;
typedef std::deque<CJednostka>::const_iterator it_Grupa;  //const_iterator


typedef std::deque<CPara> Pary;

#endif	/* CTYPES_H */

