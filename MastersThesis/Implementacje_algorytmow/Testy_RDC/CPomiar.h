/*
 *	About : 
 *	2011. The code I did for research reasons to supplement my 
 *	 	masters thesis on Częstochowa University of Technology
 *
 *	
 *	The study about:
 *		"Collision detection of irregular objects in two-dimensional Cartesian
 *		space"
 *      
 *	See:
 *	../../mgr_2.pdf
 *	
 *	Created on 05.06.2011, 
 *	
 *	Copyright (C) 2011, 2017  Rafał Stańczuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *	
 *	
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CPOMIAR_H
#define	CPOMIAR_H

#include <fstream>
#include <sstream>

using namespace std;

class CPomiar{
  public:
   CPomiar():N(0), t_zwykle(0.0), t_iteratory(0.0), liczba_obiektow(0), deep(0){} ;
   CPomiar(const CPomiar& p):N(p.N), t_zwykle(p.t_zwykle), t_iteratory(p.t_iteratory), liczba_obiektow(p.liczba_obiektow), deep(p.deep){};
   CPomiar(int lo, int N_, int deep_,  double t_zwykle_, double t_iteratory_):N(N_), t_zwykle(t_zwykle_), t_iteratory(t_iteratory_), liczba_obiektow(lo), deep(deep_){};
   int liczba_obiektow;
   int deep;
   int    N;
   double t_zwykle;
   double t_iteratory;

   ~CPomiar(){}

};


bool por_lo(const CPomiar& a, const CPomiar& b)
{
  return a.liczba_obiektow<b.liczba_obiektow;
};

bool por_N(const CPomiar& a, const CPomiar& b)
{
  return a.N<b.N;
};

CZderzenia_KUL kolizje_kul;

void analizuj_zbior(CZbior_obiektow* z){
    int l_kolizji_simple=0;

    cout<<endl<<"Analiza Simple START..."<<endl;

//    ofstream plik;
//    stringstream stmp;
//    stmp<<"out_wyniki/";
//    stmp.width(5);
//    stmp.fill('0');
//    stmp<<n;
//    stmp<<" parySimple.txt";


//    plik.open( (stmp.str()).c_str()  );
//              if(!plik)cout<<std::endl<<"...Blad zapisu do pliku..."<<std::endl;
//    plik<<"Kolizje metoda tradycyjna:  (A.id, B.id)"<<endl;

    for(int i=0; i<z->n_obiektow(); i++){

            for(int j=i+1; j<z->n_obiektow(); j++){
                if( kolizje_kul.czy_zderzenie( z->obiekt(i), z->obiekt(j) )  )
                {

                    l_kolizji_simple++;
                }
            }

    }

//    plik.close();

                 cout<<endl<<"Simple - wykryto kolizji : "<<l_kolizji_simple<<endl;

    cout<<endl<<"Analiza Simple KONIEC..."<<endl;

}

//void zapisz_zbior(CZbior_obiektow* z, int n){
//
//    ofstream plik;
//    stringstream stmp;
//    stmp<<"out_wyniki/";
//
//              stmp.width(5);
//              stmp.fill('0');
//
//   stmp<<n;
//   stmp<<" zbior.txt";
//
//
//    plik.open( (stmp.str()).c_str() );
//             if(!plik)cout<<std::endl<<"...Blad zapisu do pliku..."<<std::endl;
//    plik<<"Zbior: (x, y, z, r) "<<endl;
//
//    for(int i=0; i<z->n_obiektow(); i++){
//
//    plik<<i<<". "<<z->obiekt(i).w().x()<<" "<<z->obiekt(i).w().y()<<" "<<z->obiekt(i).w().z()<<" "<<z->obiekt(i).r()<<endl;
//
//    }
//
//    plik.close();
//}

#endif	/* CPOMIAR_H */

