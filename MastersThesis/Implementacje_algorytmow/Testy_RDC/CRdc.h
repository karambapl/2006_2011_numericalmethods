/*
 *	About : 
 *	2011. The code I did for research reasons to supplement my 
 *	 	masters thesis on Częstochowa University of Technology
 *
 *	
 *	The study about:
 *		"Collision detection of irregular objects in two-dimensional Cartesian
 *		space"
 *      
 *	See:
 *	../../mgr_2.pdf
 *	
 *	Created on 26.03.2011, 
 *	
 *	Copyright (C) 2011, 2017  Rafał Stańczuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *	
 *	
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CRDC_H
#define CRDC_H


#include "CZbior_obiektow.h"
#include "CZderzenia_KUL.h"


#include "CEnum.h"
#include "CTypes.h"

#include <algorithm>



class CRdc:public CZderzenia_KUL{
	CZbior_obiektow* obiekty;
	Grupa grupa_all;
	//Pary  _pary;
    
        void Znajdz_granice( Os, const Grupa&, Granice& );
	void Proste_przegladanie(  const Grupa& );
	void Sortuj_granice( Granice& );

        void RDC(  const Grupa&, Os, Os );
        void Oblicz_kolizje_par();
        int deep;
        
        void Znajdz_granice_iteratory( Os, const Grupa&, Granice& );
	void Proste_przegladanie_iteratory(  const Grupa& );
        void RDC_iteratory( const  Grupa&, Os, Os );

 public:
	CRdc(){};
	

	
	void Inicjuj_RDC(CZbior_obiektow* ob, int deep_)//inicjujemy raz po wygenerowaniu obiektow
	{ 
		obiekty=ob;
		grupa_all.resize( obiekty->n_obiektow() );
                deep=deep_;
               l_kolizji=0;
  	for(int i=0; i<obiekty->n_obiektow(); i++)
		{
		 grupa_all[i].id=i;
		}
                std::cout<<std::endl<<"Zainicjowano RDC...";
	}
	
	void RDC_START_zwykle()
	{
           l_kolizji=0;
	 RDC( grupa_all, OS_X, OS_Y) ; //analiza w 2D
                  //       std::cout<<std::endl<<"                RDC zwykłe- wykryto kolizji : "<<l_kolizji<<std::endl;
 	}

void RDC_START_iteratory()
	{
   l_kolizji=0;
	 RDC_iteratory( grupa_all, OS_X, OS_Y) ;//analiza w 2D
         //                std::cout<<std::endl<<"                RDC iteratory- wykryto kolizji : "<<l_kolizji<<std::endl;
	}

        int l_kolizji;
                
  ~CRdc(){}
};


bool porownaj(const CGranica& a, const CGranica& b){
	return	 a.wartosc<b.wartosc;
}

void CRdc::Znajdz_granice( Os os, const  Grupa& grupa, Granice& granice)//////////////////////////////////////////////////
	{
   // std::cout<<std::endl<<"Liczebnosc grupy: "<<grupa.size()<<" | Przewidywana liczba granic: "<<grupa.size()*2<<std::endl;
	
	  if(os==OS_X){
               //  std::cout<<"Analizowana os to: X"<<std::endl;
              
        	 for( unsigned int i=0; i<grupa.size(); i++){
                 
                 granice.push_back(   CGranica( grupa[i].id, OPEN, (obiekty->obiekt( grupa[i].id ).w().x() -  obiekty->obiekt( grupa[i].id ).r() ) ) );
                  // std::cout<<"   "<<i<<". OPEN | obiekt(id) :"<<granice.back().id<<", wartosc: "<<granice.back().wartosc<<std::endl;
                 granice.push_back(   CGranica( grupa[i].id, CLOSE, (obiekty->obiekt( grupa[i].id ).w().x() + obiekty->obiekt( grupa[i].id ).r() ) ) );
                 //  std::cout<<"   "<<i<<". CLOSE | obiekt(id) :"<<granice.back().id<<", wartosc: "<<granice.back().wartosc<<std::endl;

                 }
          }else{

              if(os==OS_Y){
                 //    std::cout<<"Analizowana os to: Y"<<std::endl;

                     for( unsigned int i=0; i<grupa.size(); i++){
                     granice.push_back(   CGranica( grupa[i].id, OPEN, (obiekty->obiekt( grupa[i].id ).w().y() - obiekty->obiekt( grupa[i].id ).r() ) ) );
                     //        std::cout<<"   "<<i<<". OPEN | obiekt(id) :"<<granice.back().id<<", wartosc: "<<granice.back().wartosc<<std::endl;
                     granice.push_back(   CGranica( grupa[i].id, CLOSE, (obiekty->obiekt( grupa[i].id ).w().y() + obiekty->obiekt( grupa[i].id ).r() ) ) );
                      //       std::cout<<"   "<<i<<". CLOSE   | obiekt(id) :"<<granice.back().id<<", wartosc: "<<granice.back().wartosc<<std::endl;

                     }
              }
              }
     
     
}///////////////////////////////////////////////////////////////////////////////

void CRdc::Znajdz_granice_iteratory( Os os, const  Grupa& grupa, Granice& granice)//////////////////////////////////////////////////
	{
   // std::cout<<std::endl<<"Liczebnosc grupy: "<<grupa.size()<<" | Przewidywana liczba granic: "<<grupa.size()*2<<std::endl;
    
	  if(os==OS_X){
                // std::cout<<"Analizowana os to: X"<<std::endl;

        	 for( it_Grupa it_grupa=grupa.begin() ; it_grupa!=grupa.end(); ++it_grupa){

                 granice.push_back(   CGranica( (*it_grupa).id, OPEN, (obiekty->obiekt( (*it_grupa).id ).w().x() -  obiekty->obiekt( (*it_grupa).id ).r() ) ) );
                  // std::cout<<"   "<<i<<". OPEN | obiekt(id) :"<<granice.back().id<<", wartosc: "<<granice.back().wartosc<<std::endl;
                 granice.push_back(   CGranica( (*it_grupa).id, CLOSE, (obiekty->obiekt( (*it_grupa).id ).w().x() + obiekty->obiekt( (*it_grupa).id ).r() ) ) );
                 //  std::cout<<"   "<<i<<". CLOSE | obiekt(id) :"<<granice.back().id<<", wartosc: "<<granice.back().wartosc<<std::endl;

                 }
          }else{

              if(os==OS_Y){
                   //  std::cout<<"Analizowana os to: Y"<<std::endl;

        	    for( it_Grupa it_grupa=grupa.begin() ; it_grupa!=grupa.end(); ++it_grupa){
                     granice.push_back(   CGranica( (*it_grupa).id, OPEN, (obiekty->obiekt( (*it_grupa).id ).w().y() - obiekty->obiekt(  (*it_grupa).id ).r() ) ) );
                     //        std::cout<<"   "<<i<<". OPEN | obiekt(id) :"<<granice.back().id<<", wartosc: "<<granice.back().wartosc<<std::endl;
                     granice.push_back(   CGranica( (*it_grupa).id, CLOSE, (obiekty->obiekt( (*it_grupa).id ).w().y() + obiekty->obiekt( (*it_grupa).id ).r() ) ) );
                      //       std::cout<<"   "<<i<<". CLOSE   | obiekt(id) :"<<granice.back().id<<", wartosc: "<<granice.back().wartosc<<std::endl;

                     }
              }
              }


}///////////////////////////////////////////////////////////////////////////////
void CRdc::Proste_przegladanie_iteratory( const Grupa& grupa){//////////////////////////////////



        for( it_Grupa it_grupa=grupa.begin() ; it_grupa!=grupa.end(); ++it_grupa){

        for( it_Grupa it_grupa2=it_grupa+1 ; it_grupa2!=grupa.end(); ++it_grupa2){
                    if(czy_zderzenie(obiekty->obiekt( (*it_grupa).id ) , obiekty->obiekt( (*it_grupa2).id )))
                    {
                       l_kolizji++;
                         //_pary.push_back( CPara(grupa[i], grupa[j], last_dist_kw()  )  );
                 //    obliczaj_kolizje(  obiekty->obiekt( (*it_grupa).id ) , obiekty->obiekt( (*it_grupa2).id ) );
                   //   std::cout<<std::endl<<(*it_grupa).id<<" x-"<<obiekty->obiekt( (*it_grupa).id ).w().x()<<" z  "<<(*it_grupa2).id<<" x-"<<obiekty->obiekt( (*it_grupa2).id ).w().x();
                     // std::cout<<std::endl<<"   diff= "<<last_dist() - (obiekty->obiekt( (*it_grupa).id ).r()+ obiekty->obiekt(  (*it_grupa2).id).r());
                    }
                  }
	 }


}

void CRdc::RDC_iteratory( const  Grupa& grupa,  Os os1, Os os2){


    //if(grupa.size()<=0)
    //    return;

  	if( ( (grupa.size()<deep) || (os1==BLEDNA_OS)  )  ) // jezeli osiagnieto minimalna liczebnosc grupy //
  	{


        	Proste_przegladanie_iteratory(grupa);


   	}
   	else{
         Granice granice;
         Znajdz_granice_iteratory( os1, grupa, granice);
         Sortuj_granice( granice );

         Grupa podGrupa;
         unsigned int count=0;

         Os nowaOs1=os2;
         Os nowaOs2=BLEDNA_OS;

         bool grupaPodzielona=false;




         for( it_Granice it_granice=granice.begin(); it_granice!=granice.end(); ++it_granice)
         {

           if (  (*it_granice).typ==OPEN )
           {
			count++;
			podGrupa.push_back( CJednostka( (*it_granice).id ) );

		   }else {
			 count--;
                             if(count==0){

                                        if (  it_granice != granice.end() -1 ){//////////////////???????????????????????????
                                                grupaPodzielona=true;

                                        }///////////////////////////////////////////////////////////////

                                        if( grupaPodzielona ){
                                             if(os1==OS_X){
                                                         nowaOs1=OS_Y;
                                                         
                                                 }
                                             if( os1==OS_Y ){
                                                         nowaOs1=OS_X;
                                                        
                                                 }
 

                                        }

                               RDC_iteratory( podGrupa, nowaOs1, nowaOs2 );
			      podGrupa.clear();
                             }
                   }
	   }

	   }
}



void CRdc::Proste_przegladanie( const Grupa& grupa){//////////////////////////////////


   
	 for( unsigned int i=0; i< grupa.size(); i++){
           
		 for( unsigned int j=i+1; j<grupa.size(); j++){
                    if(czy_zderzenie(obiekty->obiekt( grupa[i].id ) , obiekty->obiekt( grupa[j].id )))
                    {
                        l_kolizji++;
                         //_pary.push_back( CPara(grupa[i], grupa[j], last_dist_kw()  )  );
                   //   obliczaj_kolizje(  obiekty->obiekt( grupa[i].id ) , obiekty->obiekt( grupa[j].id ) );
                    //  std::cout<<std::endl<<grupa[i].id<<" x-"<<obiekty->obiekt( grupa[i].id ).w().x()<<" z  "<<grupa[j].id<<" x-"<<obiekty->obiekt( grupa[j].id ).w().x();
                    //  std::cout<<std::endl<<"   diff= "<<last_dist() - (obiekty->obiekt( grupa[i].id ).r()+obiekty->obiekt( grupa[j].id ).r());
                    }
                  }		 
	 }

	
}
	

	
void CRdc::Sortuj_granice( Granice& granice){

		std::stable_sort( granice.begin(), granice.end(), porownaj );  //ta funkcje mozna napisac w OpenMP;]
//		std::cout<<std::endl<<"    GRANICE posortowane...: ( id_obiektu, typ granicy( 1-OPEN, 2-CLOSE), wartosc )";
//		for(int i=0; i<granice.size(); i++)
//		  std::cout<<std::endl<<"      "<<i<<". "<<granice[i].id<<",  "<<granice[i].typ<<", "<<granice[i].wartosc;   //co to za nan-y ? ;/
//
//		  std::cout<<std::endl;
}
   
   
   
void CRdc::RDC( const  Grupa& grupa,  Os os1, Os os2){
 

    //if(grupa.size()<=0)
    //    return;
    
  	if( ( (grupa.size()<deep) || (os1==BLEDNA_OS)  )  ) // jezeli osiagnieto minimalna liczebnosc grupy //
  	{
   
        	Proste_przegladanie(grupa);
    	}
   	else{
         Granice granice;
         Znajdz_granice( os1, grupa, granice);
         Sortuj_granice( granice );
         
         Grupa podGrupa;
         unsigned int count=0;
         
         Os nowaOs1=os2;
         Os nowaOs2=BLEDNA_OS;

         bool grupaPodzielona=false;
        


         
         for( int i=0; i<granice.size(); i++ )
         {

           if (  granice[i].typ==OPEN )
           {
			count++;
			podGrupa.push_back( CJednostka( granice[i].id ) );
			   
           }else {
                 count--;
                     if(count==0){

                                if (  i != granice.size()-1 ){//////////////////???????????????????????????
                                        grupaPodzielona=true;

                                }///////////////////////////////////////////////////////////////

                                if( grupaPodzielona ){
                                     if(os1==OS_X){
                                                 nowaOs1=OS_Y;
           
                                         }
                                     if( os1==OS_Y ){
                                                 nowaOs1=OS_X;
                                      
                                         }
                      

                                }

                       RDC( podGrupa, nowaOs1, nowaOs2);
                      podGrupa.clear();
                     }
                   }
	   }
	
	   }
}

#endif 
