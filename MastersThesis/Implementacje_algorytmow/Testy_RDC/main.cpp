/*
 *	About : 
 *	2011. The code I did for research reasons to supplement my 
 *	 	masters thesis on Częstochowa University of Technology
 *
 *	
 *	The study about:
 *		"Collision detection of irregular objects in two-dimensional Cartesian
 *		space"
 *      
 *	See:
 *	../../mgr_2.pdf
 *	
 *	Created on 18.01.2011, 
 *	
 *	Copyright (C) 2011, 2017  Rafał Stańczuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *	
 *	
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <vector>
#include <iostream>

#include <stdio.h>
#include <stdlib.h>

#include <time.h>


#include <cmath>
#include "CWektor2D.h"

#include "CRuch.h"
#include "CZbior_obiektow.h"
#include "CStale.h"


using namespace std;

CRuch ruch;
CZbior_obiektow zbior_obiektow;

//===============================================================================================
//====================================  MAIN           ==========================================
//===============================================================================================
int main(int argc, char *argv[]){
  
 
  srand ( time(NULL) );

     zbior_obiektow.generuj(3, 0.0, 1.0,  0.5, 1.1, 0.0, 0.0);
      
    ruch.inicjuj( &zbior_obiektow );
    ruch.obliczaj();
    

    return 0;
}
