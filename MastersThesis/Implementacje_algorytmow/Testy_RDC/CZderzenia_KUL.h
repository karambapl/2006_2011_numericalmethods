/*
 *	About : 
 *	2011. The code I did for research reasons to supplement my 
 *	 	masters thesis on Częstochowa University of Technology
 *
 *	
 *	The study about:
 *		"Collision detection of irregular objects in two-dimensional Cartesian
 *		space"
 *      
 *	See:
 *	../../mgr_2.pdf
 *	
 *	
 *	Copyright (C) 2011, 2017  Rafał Stańczuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *	
 *	
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CZDERZENIA_KUL_H
#define CZDERZENIA_KUL_H


#include "CWektor2D.h"
#include "CMateria.h"
#include "CStale.h"


//=============================== klasa obslugujaca w calosci efekt odbicia kula <-> kula ============

///Klasa zawieraj�ca metody numeryczne i parametry opisuj�ce zjawisko odbicia
class CZderzenia_KUL{      
      private:


         ///zapamietanie odleglosci pomiedzy kulami                          
        double odleglosc;

        
        CWektor2D v2_2_;
        CWektor2D v1_2_;
        
        CWektor2D v1;
        CWektor2D v2;
        
        CWektor2D pol_ciala_1;
        CWektor2D pol_ciala_2;
        
        
        CWektor2D v1_n;
        CWektor2D v1_p;
        
        CWektor2D v2_n;
        CWektor2D v2_p;
        
        CWektor2D v2_n2;
        CWektor2D v1_n2;
        ///Wektor normalny
        CWektor2D n_norm;
        

	
        CWektor2D normalizuj_wektor( const CWektor2D& ); //wektor normalny akcji
               
                                                         
        void rzutuj_wektory_na_wektor_akcji(void);

        CWektor2D wektor_normalny(const float&, const float&,  double (*fun)(float, float) );




           CWektor2D roznica_v;
      public:
        
        CZderzenia_KUL():  odleglosc( 0.0 ){;};
        bool czy_zderzenie(  const CMateria& , const CMateria& );
         
        /*!
        * Oblicza warto�ci pr�dko�ci po zderzeniu si� kul zgodnie
        * z Zasad� Zachowania Energii i P�du
        * a nast�pnie przekazuje warto�ci wektor�w pr�dko�ci i po�o�enia do zmiennych prywatnych
        */
        void obliczaj_kolizje(   CMateria&,  CMateria&);
        
        void obliczaj_kolizje_z_powierzchnia( CMateria& );


        double last_dist(){return odleglosc;};
        

        
        ///Wektor pr�dko�ci drugiego cia�a po zderzeniu z pierwszym
        const CWektor2D& v2_2(void) const { return v2_2_;}//wyjscie
        ///Wektor pr�dko�ci pierwszego cia�a po zderzeniu z drugim
        const CWektor2D& v1_2(void) const { return v1_2_;} //wyjscie
        ///Wektor po�o�enia cia�a 1. po umieszczeniu go na zewn�trz 2.
        const CWektor2D& w1_2(void) const { return pol_ciala_1; }//wyjscie
        ///Wektor po�o�enia cia�a 2. po umieszczeniu go na zewn�trz 1.
        const CWektor2D& w2_2(void) const { return pol_ciala_2; }//wyjscie
        
        //Obiekt jest raz inicjowany dlatego te� trzeba zerowac dla pewno�ci po kazdym odbiciu
        void zderzenia_zeruj_wektory(void); 
                                                                                    
        ~CZderzenia_KUL(){
                     zderzenia_zeruj_wektory(); 
                     };
        
     };


///Normalizuje wektor
CWektor2D CZderzenia_KUL::normalizuj_wektor( const CWektor2D& w )
{
   
   double dl=w.dlugosc();

   return CWektor2D( w.x() / dl,  w.y() / dl );
}


bool CZderzenia_KUL::czy_zderzenie( const CMateria& m1, const CMateria& m2)
{

    odleglosc=  sqrt( (m1.w().x() - m2.w().x())*(m1.w().x() - m2.w().x()) + (m1.w().y() - m2.w().y())*(m1.w().y() - m2.w().y())    );

   return    ( odleglosc < (m1.r()+m2.r()) );
}

void CZderzenia_KUL::obliczaj_kolizje(   CMateria& mat1,  CMateria& mat2 )
{
    
              

           double tmp=  ( ( ( mat1.r()+mat2.r()  - odleglosc )  )*0.5) ;
      

         pol_ciala_2=mat2.w()  + ( mat2.w() - mat1.w() )*(tmp/odleglosc);
         pol_ciala_1=mat1.w()  + ( mat1.w() - mat2.w() )*(tmp/odleglosc);
         
         
 

      v1=mat1.v();
     v2=mat2.v();

     v1_n.dlugosc_licz();
     v2_n.dlugosc_licz();

     rzutuj_wektory_na_wektor_akcji();

     
 

   
    
     //=====oblicza zjawisko odbicia z zasady zachowania pedu i energii=========
     
 
     v2_n2= ( 2*v1_n + v2_n*( (mat2.masa()/mat1.masa()) - 1.0 ) )/ ( (mat2.masa()/mat1.masa()) + 1.0 );
     
     v1_n2= v1_n + ( (mat2.masa()/mat1.masa()) * ( v2_n - v2_n2 ) );
     
     
     //=========================================================================
     
     //====wyliczam wypadkowe skladowych prostopadlych i normalnych=============



     
     v2_2_ = v2_n2 + v2_p;

     
     v1_2_ = v1_n2 + v1_p;
     
     

                                                    
     //=========================================================================


       //przypisz wartosci aktualne
    mat1.v()=v1_2_;
    mat2.v()=v2_2_;


  
        

    mat1.w()=pol_ciala_1;
    mat2.w()=pol_ciala_2;
                                                    

                                                            
                                                             
   zderzenia_zeruj_wektory();


     
}





void CZderzenia_KUL::obliczaj_kolizje_z_powierzchnia( CMateria& materia )
{

    double tmp=0.0;

     if( tmp > ( materia.w().y() -  materia.r() ) )
     {
      CWektor2D norm;
      CWektor2D w0(0.0, 0.0);
      
      norm=CWektor2D(0.0, 1.0);
         
      v1=materia.v();
      
      v1_n= norm* ( v1%norm ) ;
   
      v1_p=(v1 - v1_n);

    
      v1_n= w0-v1_n;
        
      materia.v()= v1_n +v1_p;
      
      materia.w().y()=tmp+ materia.r();
  
     
     }
   

};

void CZderzenia_KUL::rzutuj_wektory_na_wektor_akcji(void)
{
    CWektor2D n = pol_ciala_2 - pol_ciala_1;
    
    n.dlugosc_licz();
    
    n_norm = normalizuj_wektor( n );
    
   

    v1_n=  n_norm*( v1 % n_norm );

    
    v1_p= v1 - v1_n;
    

    
    v2_n=  n_norm*( v2 % n_norm );

    
    v2_p= v2 - v2_n;
    

}



void CZderzenia_KUL::zderzenia_zeruj_wektory(void)
{
     
        odleglosc=0.0; //zapamietanie odleglosci pomiedzy kulami
        
         v2_2_.zeruj();
         v1_2_.zeruj();
        
         pol_ciala_1.zeruj();
         pol_ciala_2.zeruj();
        
         v1_n.zeruj();
         v1_p.zeruj();
        
         v2_n.zeruj();
         v2_p.zeruj();
        
         v2_n2.zeruj();
         v1_n2.zeruj();
         
         v1.zeruj();
         v2.zeruj();
         
         n_norm.zeruj();
     
}

//========================================================================================
//========================================================================================   

#endif 
