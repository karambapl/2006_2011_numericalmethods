/*
 *	About : 
 *	2011. The code I did for research reasons to supplement my 
 *	 	masters thesis on Częstochowa University of Technology
 *
 *	
 *	The study about:
 *		"Collision detection of irregular objects in two-dimensional Cartesian
 *		space"
 *      
 *	See:
 *	../../mgr_2.pdf
 *	
 *	Created on 26.03.2011, 
 *	
 *	Copyright (C) 2011, 2017  Rafał Stańczuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *	
 *	
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CGRANICA_H
#define	CGRANICA_H

class CGranica{
	public:
	CGranica():wartosc(-1.0e123), typ(INIT), id(-1){}
	CGranica( TypG t, const float& wart):typ(t), wartosc(wart){}
        CGranica( int _id, TypG t, const float& wart):id(_id), typ(t), wartosc(wart){}
	CGranica( const CGranica& g)
	{
	 typ=g.typ;
	 wartosc=g.wartosc;
	 id=g.id;
	}

	double wartosc;
	TypG typ;

        ///Id obiektu do ktorego nalezy dana granica
        int id;


	~CGranica(){}
};

#endif	/* CGRANICA_H */

