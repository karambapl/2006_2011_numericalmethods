/*
 *	About : 
 *	2011. The code I did for research reasons to supplement my 
 *	 	masters thesis on Częstochowa University of Technology
 *
 *	
 *	The study about:
 *		"Collision detection of irregular objects in two-dimensional Cartesian
 *		space"
 *      
 *	See:
 *	../../mgr_2.pdf
 *	
 *	Created on 26.03.2011, 
 *	
 *	Copyright (C) 2011, 2017  Rafał Stańczuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *	
 *	
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CPARA_H
#define	CPARA_H

class CPara{
	public:
	  CPara():odl_kw(0.0){};
	  CPara(const CPara& p)
	  {
		//A=p.A;
                A.id=p.A.id;
                B.id=p.B.id;
                odl_kw=p.odl_kw;

		//B=p.B;
	  }
	  CPara(const CJednostka& A_, const CJednostka& B_, double _odl_kw)
	  {
		  //A=A_;
                  A.id=A_.id;
		  //B=B_;
                  B.id=B_.id;
		  odl_kw=_odl_kw;
	  }
    //  CPara(CJednostka A_, CJednostka B_):A(A_), B(B_){};
	  //CPara(const CJednostka& A_, const CJednostka& B_):A(A_), B(B_){};

	CJednostka A;
	CJednostka B;

	///Kwadrat odleglosci - algorytm pierwiastkuje tylko w przypadku potrzeby analizy danej kolizji
	double odl_kw;


	  ~CPara(){}
};

#endif	/* CPARA_H */

