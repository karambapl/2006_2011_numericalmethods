/*
 *	About : 
 *	2011. The code, thats I made for research reasons to supplement my 
 *	 	masters thesis on Częstochowa University of Technology
 *
 *	
 *	The study about:
 *		"Collision detection of irregular objects in two-dimensional Cartesian
 *		space"
 *      
 *	See:
 *	../../mgr_2.pdf
 *	
 *	Created on 18.06.2011, 
 *	
 *	Copyright (C) 2011, 2017  Rafał Stańczuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *	
 *	
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>

#include "CSet.h"

#include "CWektor2D.h"

#include "time_interval.h"
#include <iomanip>
#include <vector>
#include <algorithm>

using namespace std;

void zapisz(CSet* set,  char*& nazwa){

         deque<CPoint> otoczka= *(set->get_CH());
         deque<CPoint> zbior= *(set->get_set());
         
        //================================================================================
    ofstream plik;
    plik.open(nazwa);
    int ip=1;
    int il=1;

    if(plik){


        for(int i=0; i<otoczka.size(); i++){
            plik<<endl<<"Point("<<ip<<")={"<<otoczka[i].x()<<", "<<otoczka[i].y()<<", 0};";
            ip++;
        }


            for(int i=0; i< otoczka.size()-1; i++){

               plik<<endl<<"Line("<<il<<")={"<<i+1<<", "<<i+2<<"};";
               il++;
            }

       plik<<endl<<"Line("<<il<<")={"<<ip-1<<", 1};";
       il++;

///////////////////////////////////////////////////////////////////////////////////////////////////


       for(int i=0; i<zbior.size(); i++){
           plik<<endl<<"Point("<<ip<<")={"<<zbior[i].x()<<", "<<zbior[i].y()<<", 0};";
           ip++;
       }

       //////////////////////////////////////////////////////////////////////////////

    // plik<<endl<<"Point("<<ip<<")={"<< set->get_Center().x() <<", "<<set->get_Center().y()<<", 0};";
    //   ip++;

        plik<<endl<<"Point("<<ip<<")={"<<set->get_AABB_a().x()<<", "<<set->get_AABB_a().y()<<", 0};";
           ip++;
        plik<<endl<<"Point("<<ip<<")={"<<set->get_AABB_b().x()<<", "<<set->get_AABB_a().y()<<", 0};";
           ip++;
        plik<<endl<<"Point("<<ip<<")={"<<set->get_AABB_b().x()<<", "<<set->get_AABB_b().y()<<", 0};";
           ip++;
        plik<<endl<<"Point("<<ip<<")={"<<set->get_AABB_a().x()<<", "<<set->get_AABB_b().y()<<", 0};";
           ip++;

       plik<<endl<<"Line("<<il<<")={"<<ip-4<<", "<<ip-3<<"};";
       il++;
       plik<<endl<<"Line("<<il<<")={"<<ip-3<<", "<<ip-2<<"};";
       il++;
       plik<<endl<<"Line("<<il<<")={"<<ip-2<<", "<<ip-1<<"};";
       il++;
       plik<<endl<<"Line("<<il<<")={"<<ip-1<<", "<<ip-4<<"};";
       il++;
       
     plik.close();
    }


}

void zapisz(const deque<CPoint>& otoczka, const deque<CPoint>& zbior, const deque<CPoint>& otoczka1, const deque<CPoint>& zbior1,char*& nazwa){

        //================================================================================
    ofstream plik;
    plik.open(nazwa);
    int ip=1;
    int il=1;
    
    if(plik){


        for(int i=0; i<otoczka.size(); i++){
            plik<<endl<<"Point("<<ip<<")={"<<otoczka[i].x()<<", "<<otoczka[i].y()<<", 0};";
            ip++;
        }
       
            
            for(int i=0; i< otoczka.size()-1; i++){

               plik<<endl<<"Line("<<il<<")={"<<i+1<<", "<<i+2<<"};";
               il++;
            }
        
       plik<<endl<<"Line("<<il<<")={"<<ip-1<<", 1};";
       il++;

///////////////////////////////////////////////////////////////////////////////////////////////////

        int old_ip=ip-1;
        
        for(int i=0; i<otoczka1.size(); i++){
            plik<<endl<<"Point("<<ip<<")={"<<otoczka1[i].x()<<", "<<otoczka1[i].y()<<", 0};";
            ip++;
        }


            for(int i=0; i< otoczka1.size()-1; i++){

               plik<<endl<<"Line("<<il<<")={"<<(old_ip)+1+i<<", "<<(old_ip)+2+i<<"};";
               il++;
            }
       plik<<endl<<"Line("<<il<<")={"<<ip-1<<", "<<old_ip+1<<"};";

       
       for(int i=0; i<zbior.size(); i++){
           plik<<endl<<"Point("<<ip<<")={"<<zbior[i].x()<<", "<<zbior[i].y()<<", 0};";
           ip++;
       }
       for(int i=0; i<zbior1.size(); i++){
           plik<<endl<<"Point("<<ip<<")={"<<zbior1[i].x()<<", "<<zbior1[i].y()<<", 0};";
           ip++;
       }
       //////////////////////////////////////////////////////////////////////////////



     plik.close();
    }


}

class CPomiar{

public:
    CPomiar():parametr(0.0),  t(0.0){;};
    CPomiar(const CPomiar& p):parametr(p.parametr),  t(p.t){;};
    CPomiar(const double& par, const double& _t):parametr(par),  t(_t){;};

    bool operator()(const CPomiar& a, const CPomiar& b){
        return (a.parametr)<(b.parametr);
    }

    double parametr;

    double t;

    ~CPomiar(){};
};

/*
 * 
 */
int main(int argc, char** argv) {

    CSet *new_set1=new CSet();
    deque<CPoint> *points1=new deque<CPoint>();

    CSet *new_set2=new CSet();
    deque<CPoint> *points2=new deque<CPoint>();

     //   new_set->init(points);

   
    vector<CPomiar> pom_Graham;

time_interval t;


 ////////////////////////////  POMIAR CZASU SAT w zal od liczby bokow otoczek ////////////////////
// vector<CPomiar> pom_SAT;
//
// for(int n=3; n<100; n++){
//
//     cout<<endl<<"n= "<<n<<" z 100 =======================================";
//
//     for(int in=0; in<100; in++){
//
//
//     cout<<endl<<n<<".      in= "<<in+1<<" z 100";
//
//   new_set1->generate_n(CWektor2D(0, 0), CWektor2D(1000, 1000), n);
//   new_set2->generate_n(CWektor2D(500, 500), CWektor2D(2000, 2000), n);
//
//   deque<CPoint> zbior1=*(new_set1->get_set());
//   deque<CPoint> zbior2=*(new_set2->get_set());
//
//
//   new_set1->compute_CH();
//   new_set2->compute_CH();
//
//   double tmp=0.0;
//
//   for(int i=0; i<10000; i++){
//
//       t.start();
//        new_set1->is_Colliding(new_set2);
//       t.stop();
//       tmp+=t.value();
//   }
//   tmp/=10000.0;
//
//     pom_SAT.push_back( CPomiar( new_set1->get_CH()->size()+ new_set2->get_CH()->size(), tmp ) );
//
//
//
// }
// }
//     sort(pom_SAT.begin(), pom_SAT.end(), CPomiar());
//
//     ofstream plik_pom;
//     plik_pom.open("pomiar_sat.txt");
//
//     plik_pom<<endl<<"Sredni Czas[s] 10e4 prob w zaleznosci od ilosci bokow w obydwu otoczkach: "<<endl;
//
//     for(vector<CPomiar>::iterator it=pom_SAT.begin(); it!=pom_SAT.end(); it++){
//         plik_pom<<endl<< it->parametr<<" "<<it->t;
//
//     }
//
//     plik_pom.close();
//////////////////////////////////////////////////////////////////////////////////////////////////////////






///////////////////////////////////     Prezentacja działania algorytmu Grahama         //////////////////////
//    new_set1->generate_n(CWektor2D(0, 0), CWektor2D(1000, 1000), 5);
//    new_set1->compute_CH();
//
//    cout<<endl<<"Dla losowego zbioru "<<new_set1->get_set()->size()<<" punktow, otoczka dla niej ma  : "
//              <<new_set1->get_CH()->size()<<" boków/wierzcholkow."<<endl;
//
//    zapisz(new_set1, argv[1]);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
///////////////////////////////// Prezentacja wyznaczonego AABB  /////////////////////
//
//   new_set1->generate_n(CWektor2D(0, 0), CWektor2D(1500, 1500), 10);
//   new_set1->compute_CH();
//
//   zapisz(new_set1, argv[1]);
//////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////    Prezentacja dzialania algorytmu SAT ///////////////
//   new_set1->generate_n(CWektor2D(0, 0), CWektor2D(1500, 1500), 10);
//   new_set2->generate_n(CWektor2D(500, 500), CWektor2D(2000, 2000), 2);
//
//   new_set2->compute_CH();
//   new_set1->compute_CH();
//   cout<<endl<<"Wielokat A, o "<<new_set1->get_CH()->size()<<" bokach,"<<endl;
//
//   if(new_set1->is_Colliding(new_set2)){
//       cout<<"przecina(koliduje)";
//   }else{
//       cout<<"nie przecina się";
//   }
//
//    cout<<endl<<"z Wielokatem B, o "<<new_set2->get_CH()->size()<<" bokach."<<endl;
//
//   zapisz( *(new_set1->get_CH()), *(new_set1->get_set()), *(new_set2->get_CH()), *(new_set2->get_set()), argv[1]);
//////////////////////////////////////////////////////////////////////////////////////










    
////////////////////////  Pomiar czasu dzialania algorytmu Grahama w zaleznosci od liczby punktow wejsciowych ////////////



vector<CPomiar> pom_graham;

 for(int n=10; n<1010; n+=10){
     cout<<endl<<n<<" z 1000";




   double tmp=0.0;

   for(int i=0; i<1000; i++){
     new_set1->generate_n(CWektor2D(0, 0), CWektor2D(1000, 1000), n*10);
       t.start();
         new_set1->compute_CH();
       t.stop();
       tmp+=t.value();
   }
   tmp/=1000.0;

     pom_graham.push_back( CPomiar( new_set1->get_set()->size(), tmp ) );



 }

     sort(pom_graham.begin(), pom_graham.end(), CPomiar());

     ofstream plik_pom;
     plik_pom.open("pomiar_graham.txt");

     plik_pom<<endl<<"Sredni Czas[s] 1000 prob czasu dzialania algorytmu Grahama w zaleznosci od ilosci punktow wejsciowych: "<<endl;

     for(vector<CPomiar>::iterator it=pom_graham.begin(); it!=pom_graham.end(); it++){
         plik_pom<<endl<< it->parametr<<" "<<it->t;

     }

     plik_pom.close();
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        cout<<endl<<endl;
    
    
    return 0;
}

