/*
 *	About : 
 *	2011. The code I did for research reasons to supplement my 
 *	 	masters thesis on Częstochowa University of Technology
 *
 *	
 *	The study about:
 *		"Collision detection of irregular objects in two-dimensional Cartesian
 *		space"
 *      
 *	See:
 *	../../mgr_2.pdf
 *	
 *	Created on 19.06.2011, 
 *	
 *	Copyright (C) 2011, 2017  Rafał Stańczuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *	
 *	
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FUNCTIONS_H
#define	FUNCTIONS_H

#include "Constants.h"
#include "CPoint.h"
#include "CWektor2D.h"

bool comp_dlugosc( const CPoint& a,  const CPoint& b){
    return a.dlugosc()<b.dlugosc();
}

double dabs(double a){
    return (a>0.0)?a:-a;
}

bool equal2D(const CPoint& a,  const CPoint& b){
    CWektor2D diff(a.x()-b.x(), a.y()- b.y());

    return ( (  dabs( diff.x() )<_EPS_   )&& (  dabs( diff.y() )<_EPS_   ) );
}

bool equal(const double& a,  const double& b){
    double d=a-b;

    return dabs(d)<_EPS_   ;
}

bool comp_yx(const CPoint& a,  const CPoint& b){
   if(equal(a.y(), b.y())){
       return a.x()<b.x();
   }else
       return a.y()<b.y();

}

bool comp_leksykograph( const CPoint& a,  const CPoint& b){

     if (a.alfa()<b.alfa())      return true;
      else if (a.alfa()>b.alfa()) return false;
      else if (equal2D(a,b))     return false;
      else if (a.dlugosc() < b.dlugosc())
         return true;
      else
         return false;
}

///sinus kata pomiedzy prosta ab, a punktem c
double sin_points(const CPoint& a, const CPoint& b, const CPoint& c){
    return  ( b.x()-a.x() )*(c.y() - a.y())  - (c.x()-a.x())*(b.y()-a.y());
}



#endif	/* FUNCTIONS_H */

