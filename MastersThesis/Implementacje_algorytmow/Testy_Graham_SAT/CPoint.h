/*
 *	About : 
 *	2011. The code I did for research reasons to supplement my 
 *	 	masters thesis on Częstochowa University of Technology
 *
 *	
 *	The study about:
 *		"Collision detection of irregular objects in two-dimensional Cartesian
 *		space"
 *      
 *	See:
 *	../../mgr_2.pdf
 *	
 *	Created on 19.06.2011, 
 *	
 *	Copyright (C) 2011, 2017  Rafał Stańczuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *	
 *	
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CPOINT_H
#define	CPOINT_H

#include "CWektor2D.h"

class CPoint:public CWektor2D{
public:
    CPoint():id(0){};
    CPoint(const CWektor2D& w, int _id):CWektor2D(w), id(_id){;}
    CPoint(double _x, double _y, int _id){
        x()=_x;
        y()=_y;
        id=_id;
    }
    CPoint(const CPoint& p):id(p.id){
        x()=p.x();
        y()=p.y();
        alfa()=p.alfa();
        dlugosc()=p.dlugosc();
    }

    void set(const CPoint& p){
        x()=p.x();
        y()=p.y();
        alfa()=p.alfa();
        dlugosc()=p.dlugosc();
        id=p.id;
    }

    int id;

    ~CPoint(){;};
};


#endif	/* CPOINT_H */

