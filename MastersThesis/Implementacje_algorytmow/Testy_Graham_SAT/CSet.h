/*
 *	About : 
 *	2011. The code I did for research reasons to supplement my 
 *	 	masters thesis on Częstochowa University of Technology
 *
 *	
 *	The study about:
 *		"Collision detection of irregular objects in two-dimensional Cartesian
 *		space"
 *      
 *	See:
 *	../../mgr_2.pdf
 *	
 *	Created on 18.06.2011, 
 *	
 *	Copyright (C) 2011, 2017  Rafał Stańczuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *	
 *	
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CSET_H
#define	CSET_H

#include "CWektor2D.h"
#include "Functions.h"
#include "CPoint.h"
#include "Constants.h"

#include <time.h>
#include <stdlib.h>

#include <deque>
#include <algorithm>


using namespace std;






class CSet{

    

    deque<CPoint> *otoczka;
    deque<CPoint> *punkty;
    deque<CPoint> *punkty_norm;
    CPoint set0;

    ///Granice AABB
    CWektor2D AABB_a, AABB_b;

    double R;

    void compute_AABB();

    void normalize();
    
    void compute_centroid();
    

public:
    CSet():R(0.0){
        srand(time(NULL));
        otoczka=new deque<CPoint>();
        punkty_norm=new deque<CPoint>();
        punkty=new deque<CPoint>();
    };

    ///Obliczaj Convex Hull
    void compute_CH();

    ///Wspolzedne granicy wspolzednych losowanych obiektow i ich liczba
    void generate_n(const CWektor2D&, const CWektor2D&, int);

    void init(deque<CPoint>* points){
        punkty_norm=points;

    compute_centroid();

    normalize();

    compute_AABB();

         ///Zapisuj do listy prywatnej
     ///- będzie użyte do sortowania leksykograficznego w algorytmie Grahama
     sort( punkty_norm->begin(), punkty_norm->end(), comp_dlugosc );
    }

    CWektor2D& get_AABB_a(){ return AABB_a; }
    CWektor2D& get_AABB_b(){ return AABB_b; }

    deque<CPoint>* get_set(){ return punkty; }
    deque<CPoint>* get_norm(){ return punkty_norm; }
    deque<CPoint>* get_CH(){ return otoczka; }

    bool is_Colliding(CSet* s);

    CWektor2D& get_Center(){ return set0; }

    double& get_R(){ return R; }

    ~CSet(){  };
};




void CSet::compute_CH(){



//================================================ Sortowanie leksykograficzne  ========================
  //  cout<<endl<<"Przed sortowaniem: ";
    for(int i=0; i<punkty_norm->size(); i++){
        (*punkty_norm)[i].alfa_licz();
      //  cout<<endl<<(*punkty_norm)[i].id<<". dl="<<(*punkty_norm)[i].dlugosc()<<", alfa="<<(*punkty_norm)[i].alfa();
    }

    sort(punkty_norm->begin(), punkty_norm->end(), comp_leksykograph);

//      ofstream p;
//  p.open("posortowane.txt");
//
//    cout<<endl<<endl<<"Po posortowaniu: ";
// for(int i=0; i<punkty_norm.size(); i++){
//  cout<<endl<<i+1<<"| id="<<punkty_norm[i].id<<". ; (x,y)=("<<punkty_norm[i].x()<<", "<<punkty_norm[i].y()<<"); "<<". dl="<<punkty_norm[i].dlugosc()<<", alfa="<<punkty_norm[i].alfa();
//
//
//
//            p<<endl<<"Point("<<i+1<<")={"<<punkty_norm[i].x()<<", "<<punkty_norm[i].y()<<", 0};";
//
//
// }
//       p<<endl<<"Point(0)={0, 0, 0};";
//
//    p.close();
  //======================================================================================================







    deque<CPoint>::iterator it0,itt;


    it0=punkty_norm->begin();

    for(itt=it0+1; itt<punkty_norm->end(); itt++){
        if( !comp_yx( *it0, *itt  ) ){
            it0=itt;
        }
    }

  //  cout<<endl<<"Wybieram jako punkt poczatkowy: "<<(*it0).id<<". "<<endl<<endl;
 //======================================================================================================


    deque<CPoint>::iterator it;
    deque<CPoint> otoczka_tmp;
 
    otoczka_tmp.push_back(*it0); //ten punkt nalezy do otoczki

    for(it=it0+1; it!=punkty_norm->end(); it++){
        otoczka_tmp.push_back(*it);
    }

    for(it=punkty_norm->begin(); it!=it0; it++){
        otoczka_tmp.push_back(*it);
    }

    otoczka_tmp.push_back(*it0);




//    cout<<endl;
// for(int i=0; i<otoczka_tmp.size(); i++){
//  cout<<endl<<i+1<<"| id="<<otoczka_tmp[i].id<<". ; (x,y)=("<<otoczka_tmp[i].x()<<", "<<otoczka_tmp[i].y()<<"); "<<". dl="<<otoczka_tmp[i].dlugosc()<<", alfa="<<otoczka_tmp[i].alfa();
//
// }



deque<CPoint>::iterator  it_next, it_next_next;


it=otoczka_tmp.begin();
it_next=it+1;
it_next_next=it+2;


while( it_next!= otoczka_tmp.end()-1 ){

    if( sin_points(*it, *it_next_next, *it_next)>0 ){

        otoczka_tmp.erase(it_next);

        if(it>otoczka_tmp.begin()+1)
            it--;

        it_next=it+1;
        it_next_next=it+2;

        

        
    }else{
        it++;
        it_next=it+1;
        it_next_next=it+2;
        
    }

    
}




                       otoczka->clear();

                       for(it=otoczka_tmp.begin(); it!=otoczka_tmp.end()-1; it++){//nie dubluj pierwszego punktu
                           it->w() = it->w()+ set0; //powrot do globlanego ukladu wspolzednych
                           otoczka->push_back( *it );
                       }
    
 
//================================

}

void CSet::compute_AABB(){
    
    double minx=10e10, maxx=-10e10, tmpx=0.0;
    double miny=10e10, maxy=-10e10, tmpy=0.0;


    for(int i=0; i<punkty->size(); i++){
        tmpx=(*punkty)[i].x();
        tmpy=(*punkty)[i].y();

        if( tmpx<minx )minx=tmpx;
        if( tmpy<miny )miny=tmpy;

        if( tmpx>maxx )maxx=tmpx;
        if( tmpy>maxy )maxy=tmpy;
    }


    AABB_a=CWektor2D(minx, miny);
    AABB_b=CWektor2D(maxx, maxy);

}

void CSet::compute_centroid(){

    set0.x()=0.0;
    set0.y()=0.0;
    
    if( punkty->size()!=0 ){
        for(int i=0; i<punkty->size(); i++){
            set0.x()+=(*punkty)[i].x();
            set0.y()+=(*punkty)[i].y();
        }
            set0.x()/=(*punkty).size();
            set0.y()/=(*punkty).size();
    }
   // cout<<endl<<"Centroid: (x, y) = ("<<set0.x()<<", "<<set0.y()<<")"<<endl;
}

void CSet::normalize(){

 for(deque<CPoint>::iterator i=punkty->begin(); i!=punkty->end(); i++){
        punkty_norm->push_back( CPoint( CWektor2D( i->x()-set0.x(), i->y()-set0.y() ),  i->id )  );
        punkty_norm->back().dlugosc_licz();
    }
}

void CSet::generate_n(const CWektor2D& a, const CWektor2D& b, int n){

    punkty->clear();
    punkty_norm->clear();
    otoczka->clear();

   // cout<<endl<<"Punkty przed normalizacja: "<<endl;
    
    for(int i=0; i<n; i++){
        punkty->push_back( CPoint(
        CWektor2D(
           (rand()%20000/20000.0)*( b.x()-a.x() ) +a.x(),
           (rand()%20000/20000.0)*( b.y()-a.y() ) +a.y() ),
                i )

        );

       // cout<<endl<<i<<". x="<<(*punkty)[i].x()<<", y="<<(*punkty)[i].y();
        
    }


 //   cout<<endl<<"============";

    compute_centroid();

    normalize();

    compute_AABB();

         ///Zapisuj do listy prywatnej
     ///- będzie użyte do sortowania leksykograficznego w algorytmie Grahama
     sort( punkty_norm->begin(), punkty_norm->end(), comp_dlugosc );


 }

bool CSet::is_Colliding(CSet* s){

    deque<CPoint>* otoczka2 = s->get_CH();

   
    CWektor2D norm_axis;

    double tmp=0.0;
    double min0=0.0, max0=0.0;
    double min1=0.0, max1=0.0;

    double sep0=0.0, sep1=0.0;

    deque<CPoint>::iterator it0, it1;


   
    
    //Petla po normalnych bokow lokalnego wielokata
    for(it0=otoczka->begin(); it0!=otoczka->end()-1; it0++){
        norm_axis.wprowadz( -(   (it0+1)->y() - it0->y()   ), (   (it0+1)->x() - it0->x()   ) );
        norm_axis.normalizuj();

        min0= norm_axis%( (otoczka->begin())->w() );
        max0= min0;
        for(it1=otoczka->begin()+1; it1!=otoczka->end(); it1++){
            tmp=norm_axis%( (it1)->w() );
            if (tmp< min0) min0=tmp;
            if (tmp> max0) max0=tmp;
        }

        min1= norm_axis%( (otoczka2->begin())->w() );
        max1= min1;
        for(it1=otoczka2->begin()+1; it1!=otoczka2->end(); it1++){
            tmp=norm_axis%( (it1)->w() );
            if (tmp<min1) min1=tmp;
            if (tmp>max1) max1=tmp;
        }


        sep0=min0-max1;
        sep1=min1-max0;

        if( (sep0>0.0)||(sep1>0.0) ){
            return false;
        }

    }


    //Petla po normalnych bokow otoczka2 wielokata (z parametru)
    for(it0=otoczka2->begin(); it0!=otoczka2->end()-1; it0++){
        norm_axis.wprowadz( -(   (it0+1)->y() - it0->y()   ), (   (it0+1)->x() - it0->x()   ) );
        norm_axis.normalizuj();
    
        min0= norm_axis%( (otoczka->begin())->w() );
        max0= min0;
        for(it1=otoczka->begin()+1; it1!=otoczka->end(); it1++){
            tmp=norm_axis%( (it1)->w() );
            if (tmp< min0) min0=tmp;
            if (tmp> max0) max0=tmp;
        }

        min1= norm_axis%( (otoczka2->begin())->w() );
        max1= min1;
        for(it1=otoczka2->begin()+1; it1!=otoczka2->end(); it1++){
            tmp=norm_axis%( (it1)->w() );
            if (tmp<min1) min1=tmp;
            if (tmp>max1) max1=tmp;
        }



        sep0=min0-max1;
        sep1=min1-max0;

        if( (sep0>0.0)||(sep1>0.0) ){
            return false;
        }
    }
   
    //Nie znaleziono osi dzielacej dwa wielokaty- otoczki wypukle - czyli koliduja ze soba
    return true;
}
#endif	/* CSET_H */
