/*
 *	About : 
 *	2011. The code I did for research reasons to supplement my 
 *	 	masters thesis on Częstochowa University of Technology
 *
 *	
 *	The study about:
 *		"Collision detection of irregular objects in two-dimensional Cartesian
 *		space"
 *      
 *	See:
 *	../../mgr_2.pdf
 *	
 *	Created on 26.03.2011, 
 *	
 *	Copyright (C) 2011, 2017  Rafał Stańczuk (stanczuk.rafal@gmail.com)
 *					https://bitbucket.org/karambapl/
 *	
 *	
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CWektor2D_H
#define CWektor2D_H

#include <cmath>
#include "Constants.h"

class CWektor2D{
      double _x, _y;
      double _alfa;
      double _dl;
  public:
       CWektor2D():_x(0.0), _y(0.0),  _dl(0.0), _alfa(0.0){;};
       CWektor2D(const double& __x, const double& __y):_x(__x), _y(__y), _dl(0.0), _alfa(0.0){;};
       CWektor2D(const CWektor2D& w):_x(w.x()), _y(w.y()), _dl(w.dlugosc()), _alfa(w.alfa()){;};
       
       const double& x()const{return _x;}
       const double& y()const{return _y;}
       const double& alfa()const{return _alfa;}

       double& x(){return _x;}
       double& y(){return _y;}
       double& alfa(){return _alfa;}

        void wprowadz(const  double& a, const  double& b)
       {
         _x=a;
         _y=b; 
       }


       ///Liczy dlugosc wektora
       void dlugosc_licz(void)
       {
        _dl=sqrt( _x*_x + _y*_y  );
       }; 

       void normalizuj();

       void alfa_licz();

       void zeruj(void)
       { _x=0.0;
         _y=0.0;
       };
       
       ///Przekazuje dlugosc wektora, uprzednio policzonego
       const double& dlugosc( void ) const { return _dl;};
       double& dlugosc( void ){ return _dl;};
       
       CWektor2D& w(){ return *this;}

        ///Iloraz wektora i skalara
        friend CWektor2D operator /( const CWektor2D& , const  double& );
        ///Iloraz skalaru i wektoru 
        friend CWektor2D operator /( const  double&, const CWektor2D&  );
        ///Iloczyn skalarny wektorow
        friend  double operator %( const CWektor2D& , const CWektor2D& );
        ///Iloczyn skalara i wektora      
        friend CWektor2D operator *( const  double& , const CWektor2D& );
        ///Iloczyn wektora i skalara        
        friend CWektor2D operator *( const CWektor2D& , const double& );
        ///Suma dw�ch wektorow
        friend CWektor2D operator +(  const CWektor2D& , const CWektor2D& );
        ///R��nica dw�ch wektor�w
        friend CWektor2D operator -(  const CWektor2D& , const CWektor2D& );


         ///Operator przyrownania dwoch wektorow
//         CWektor2D& operator =(  const CWektor2D&  );//Aby posortowac nie moze byc w klasie operatora =
         

	
       ~CWektor2D(){;};
};

void CWektor2D::normalizuj(){
    dlugosc_licz();

    _x=_x/_dl;
    _y=_y/_dl;

}

void CWektor2D::alfa_licz(){


           ///I. Cwiartka
           if( _x>0.0 && _y>0.0){
               _alfa=atan(  _y/_x  )*_180DivM_PI_;
               return;
           }

           ///II.Cwiartka
            if( _x<0.0 && _y>0.0){
               _alfa=atan(  -_x/_y  )*_180DivM_PI_+ 90.0;
               return;
           }

           ///III.Cwiartka
            if( _x<0.0 && _y<0.0){
               _alfa=atan(  _y/_x  )*_180DivM_PI_+ 180.0;
               return;
           }

           ///IV.Cwiartka
            if( _x>0.0 && _y<0.0){
               _alfa=atan(  -_x/_y  )*_180DivM_PI_+ 270.0;//1.5 wart PI
               return;
           }

           if( _x==0.0 && _y>0.0){
               _alfa= 90.0;
               return;
           }
           if( _x==0.0 && _y<0.0){
               _alfa= 270.0;
               return;
           }
           if( _x>0.0 && _y==0.0){
               _alfa= 0.0;
               return;
           }
           if( _x<0.0 && _y==0.0){
               _alfa= 180.0;
               return;
           }


       };




    
//CWektor2D& CWektor2D::operator =( const CWektor2D& w )
//{
// if( &w!=this )
//     {
//      _x=w.x();
//      _y=w.y();
//     }
// return *this;
//
//}

CWektor2D operator /( const CWektor2D& a, const  double& b)
{
         return CWektor2D( ( a.x() / b ), ( a.y() / b ) );
}

CWektor2D operator /(  const  double& a, const CWektor2D& b)
{
         return CWektor2D( ( a / b.x() ), ( a / b.y() ));
}

CWektor2D operator *( const CWektor2D& w, const  double& a)
{
         return CWektor2D( a*w.x(), a*w.y() );
}

CWektor2D operator *(  const  double& a, const CWektor2D& w)
{
         return CWektor2D( a*w.x(), a*w.y() );
}
 
double operator %( const CWektor2D& a, const CWektor2D& b)
{
 return ( a.x()*b.x() + a.y()*b.y()  );      
}

CWektor2D operator +(  const CWektor2D& a, const CWektor2D& b)
{
 return CWektor2D( a.x()+b.x(), a.y()+b.y() );
}

CWektor2D operator -(  const CWektor2D& a, const CWektor2D& b)
{
 return CWektor2D( a.x()-b.x(), a.y()-b.y() );
}
        

#endif //CWektor2D_H


//===============================================================================================
//===============================================================================================
//===============================================================================================
